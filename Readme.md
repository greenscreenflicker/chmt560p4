#Usage Instruction

## KiCad generation
1. Generate the Panel in KiCad
2. Put the reference point to the lower left. The reference point setting can be found at the sidebar
3. If you want to include Finducals, mark them as "FID1", "FID2","FID3". You need one in the left low, left high and right low. Make sure to mark them as smd components
4. Create the output as a pos file. CSV is currently not supported.


## Nozzels
Basically the CHMT560 uses the Yuki 50x nozzels. Nozzels in the Machine are labeled as follow: 501 is nozzle 1 for example.
The nozzles are used according to the following list
[Juki Application manual, page 5] (http://avipre.com/products/smt&tht/juki//Juki%20-%20Nozzle%20Catalogue-Rev-C3.pdf)
* 501: 0201
* 502: 0402
* 503: 0603, 0805, SOT323
* 504: 1206
* 505: 1210,2010,2512


## Calibration

As previously noted, make sure you place the orgin in the lower left.
You can set the auxilliary axsis in Kicad using "place auxilliary axeses" in the right side menu.
The chmt560 will select the 3 points, that are close to max x, close to maxy, and close to zero point.

The fiducials must be arranged in the following way:

| -            | -            |
|--------------|--------------|
| FID1 (Mark1) | -            |
| FID3 (Mark3) | Fid2 (Mark2) |


## Default rotations for KICAD

| Package | Rotation |
|---------|----------|
| 0603    | 0        |
| 0805    | 0        |
| 1206    | 0        |
| SMC     | 0        |
| SOT23-x | 180      |
| TSSOP-x | -90      |
| LQFP    | 180      |
