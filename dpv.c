/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "unified.h"

void dpv_export_stdout(void){
    dpv_export(stdout,"stdoutput");
}

void dpv_export_filename_test(void){
    dpv_export_filename("test.dpv");
}

void dpv_export_filename(char *filename){
    FILE *fp=fopen(filename, "w");
    dpv_export(fp,filename);
    fclose(fp);
}


int dpv_replacechar(char *str, char orig, char rep) {
    char *ix = str;
    int n = 0;
    while((ix = strchr(ix, orig)) != NULL) {
        *ix++ = rep;
        n++;
    }
    return n;
}

double dpv_adjust_angle(double angle){
    while(angle>180) angle-=360.0;
    while(angle<-180) angle+=360.0;
    return angle;
}

int dpv_get_numbers_feeders_unassigned(void){
    const char *sqlstring;
    sqlite3_stmt *stmt;
    sqlstring="SELECT count(*) FROM components WHERE status%2=0 AND nozzle_id<0";
    int rc=sqlite3_prepare_v2(glob_db,sqlstring, -1, &stmt, NULL);
        if (rc != SQLITE_OK) {
        fprintf(stderr, "dpv_get_numbers_feeders_unassigned: Failed to fetch data: %s\n", sqlite3_errmsg(glob_db));
        fflush(stderr);
        return -1;
    }   
    rc = sqlite3_step (stmt);  
    if(rc != SQLITE_ROW){
        fprintf(stderr,"dpv_get_numbers_feeders_unassigned: failed to get results of unassigned feeders.");
        fflush(stderr);
        return -1;
    }
    int num=sqlite3_column_int(stmt,0);
    sqlite3_finalize(stmt);
    return num;
}

void dpv_export_feeders(FILE *fp){
        //expot feeders
    fprintf(fp,"\nTable,No.,ID,DeltX,DeltY,FeedRates,Note,Height,Speed,Status,SizeX,SizeY,HeightTake,DelayTake,Head\n");
    //fprintf(fp,)
    
    sqlite3_stmt *stmt;
    char sqlstring[500];
    strcpy(sqlstring,"SELECT comp_id,feeder_id,deltaX,deltaY,FeedRate,value,package,height,speed,status,sizeX,sizeY,heighttake,delayTake,phead.head_id+1 FROM components   INNER JOIN phead ON (components.nozzle_id=phead.nozzle_id) Order by feeder_id");
    int rc=sqlite3_prepare_v2(glob_db,sqlstring, -1, &stmt, NULL);
        if (rc != SQLITE_OK) {
        fprintf(stderr, "DPV-Export: Failed to fetch data: %s\n", sqlite3_errmsg(glob_db));
        return;
    }   


    do{
        rc = sqlite3_step (stmt);  
        if(rc != SQLITE_ROW) break;
               //Station,0,  1, 0,  0, 4,AD2-C0805,0.5,0,4,0,0,0,0
        
        fprintf(fp,"Station,%i,%i,%s,%s,%i,%s|%s,%s,%i,%i,%i,%i,%i,%s,%i\n",
                sqlite3_column_int(stmt,1),
                sqlite3_column_int(stmt,1),
                sqlite3_column_text(stmt,2),
                sqlite3_column_text(stmt,3),
                sqlite3_column_int(stmt,4),
                sqlite3_column_text(stmt,5),sqlite3_column_text(stmt,6), //note
                sqlite3_column_text(stmt,7), //height
                sqlite3_column_int(stmt,8),  //speed
                sqlite3_column_int(stmt,9),  //status
                sqlite3_column_int(stmt,10), //sizeX
                sqlite3_column_int(stmt,11), //sizeY
                sqlite3_column_int(stmt,12), //height
                sqlite3_column_text(stmt,13), //delaytake
                sqlite3_column_int(stmt,14) //Head id
                );
    }while(1);
    sqlite3_finalize(stmt);
}

void dpv_export(FILE *fp , char *chmt_filename){

    const char *sqlstring;
    
    //Header 
    fprintf(fp,"separated\n");
    fprintf(fp,"FILE,%s\n",chmt_filename);
    fprintf(fp,"PCBFILE,%s\n",chmt_filename);
    
    //https://www.tutorialspoint.com/c_standard_library/c_function_strftime.htm
    time_t timer;
    struct tm* tm_info;
    char day[3];
    char month[3];
    char year[5];
    char hour[3];
    char min[3];
    char sec[3];
    time(&timer);
    tm_info = localtime(&timer);
    strftime(day, 3, "%d", tm_info);
    strftime(month, 3, "%m", tm_info);
    strftime(year, 5, "%Y", tm_info);
    
    strftime(hour, 3, "%H", tm_info);
    strftime(min, 3, "%M", tm_info);
    strftime(sec, 3, "%S", tm_info);
     
    fprintf(fp,"DATE,%s/%s/%s\n", year,month,day);
    fprintf(fp,"TIME,%s:%s:%s\n", hour,min,sec);
    fprintf(fp,"SysVer,");
    fprintf(fp,"PANELYPE,0\n");
    
    //export feeders
    dpv_export_feeders(fp);
    
    //panels are curently not supported
    
    fprintf(fp,"\nTable,No.,ID,DeltX,DeltY\n"
            "Panel_Coord,0,1,0,0\n");
    sqlite3_stmt *stmt;
    sqlstring="SELECT bom.placement_order+1,phead.head_id,components.feeder_id,bom.deltaX,bom.deltaY,bom.angle,components.height,components.FeedRate,bom.skip,components.speed,bom.designator,components.package,components.value,components.status FROM bom LEFT JOIN components ON (bom.package=components.package AND bom.value=components.value)  LEFT JOIN phead ON (components.nozzle_id=phead.nozzle_id) WHERE designator NOT LIKE 'FID%' ORDER BY bom.placement_order";
    int rc=sqlite3_prepare_v2(glob_db,sqlstring, -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(glob_db));
        return;
    }   

    fprintf(fp,"\nTable,No.,ID,PHead,STNo.,DeltX,DeltY,Angle,Height,Skip,Speed,Explain,Note,Delay\n");
    int countup=0;
    char *posx_char=malloc(100);
    char *posy_char=malloc(100);
    char *angle_char=malloc(100);
    char *height_char=malloc(100);
    do{
        rc = sqlite3_step (stmt);  
        if(rc != SQLITE_ROW) break;  

        snprintf(posx_char,100,"%g", sqlite3_column_double(stmt,3));
        dpv_replacechar(posx_char,',','.');
        snprintf(posy_char,100,"%g", sqlite3_column_double(stmt,4));
        dpv_replacechar(posy_char,',','.');
        snprintf(angle_char,100,"%g", dpv_adjust_angle(sqlite3_column_double(stmt,5)));
        dpv_replacechar(angle_char,',','.');
        snprintf(height_char,100,"%g", sqlite3_column_double(stmt,6));
        dpv_replacechar(height_char,',','.');
        //fprintf(fp,"value:%s\n",sqlite3_column_text(stmt,12));
    
        fprintf(fp,"EComponent,%i,%i,%i,%i,%s,%s,%s,%s,%i,%i,%s,%s|%s,0\n",
                countup++, //number
                sqlite3_column_int(stmt,0), //id,
                sqlite3_column_int(stmt,1)+1, //placement head
                sqlite3_column_int(stmt,2), //feeder
                posx_char, //deltaX
                posy_char, //deltaY
                angle_char, //angle
                height_char, //hight
                sqlite3_column_int(stmt,8) | sqlite3_column_int(stmt,13), //skip
                sqlite3_column_int(stmt,9), //speed
                sqlite3_column_text(stmt,10), //designator
                sqlite3_column_text(stmt,12), //
                sqlite3_column_text(stmt,11) //
                );
    }while(1);
    sqlite3_finalize(stmt);

    
    //here we put the nozzels
    //Table,No.,ID,NozzleType
    //Nozzle,0,1,2
    //Nozzle,1,2,2
    //Nozzle,2,3,2
    //Nozzle,3,4,2
    
    fprintf(fp,"\nTable,No.,ID,NozzleType\n");
    sqlstring="SELECT nozzle_id FROM phead ORDER BY head_id;";
    rc=sqlite3_prepare_v2(glob_db,sqlstring, -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "Failed to fetch nozzle-data from phead: %s\n", sqlite3_errmsg(glob_db));
        return;
    }  
    
    int nozzle_id_counter=0;
    do{
        rc = sqlite3_step (stmt);  
        if(rc != SQLITE_ROW) break;
        //Nozzle,2,3,2
        fprintf(fp,"Nozzle,%i,%i,%i\n",nozzle_id_counter,nozzle_id_counter+1,sqlite3_column_int(stmt,0));
        nozzle_id_counter=nozzle_id_counter+1;
        
    }while(1);
    for(;nozzle_id_counter<4;nozzle_id_counter++){
        fprintf(fp,"Nozzle,%i,%i,%i\n",nozzle_id_counter,nozzle_id_counter+1,0);
    }
    sqlite3_finalize(stmt);
    /*
    Table,No.,nType,nAlg,nFinished
    PcbCalib,0,0,0,0
    Table,No.,ID,offsetX,offsetY,Note
    CalibPoint,0,0,0,0,CalibPoint,1,0,0,0,
    Table,No.,DeltX,DeltY,AlphaX,AlphaY,BetaX,BetaY,DeltaAngle
    CalibFator,0,0,0,0,0,1,1,0 
    fprintf(fp,"\nTable,No.,nType,nAlg,nFinished\n");
    fprintf(fp,"PcbCalib,0,0,0,0\n");
    fprintf(fp,"\nTable,No.,ID,offsetX,offsetY,Note\n");
    fprintf(fp,"CalibPoint,0,0,0,0,CalibPoint,1,0,0,0,\n");
    fprintf(fp,"\nTable,No.,DeltX,DeltY,AlphaX,AlphaY,BetaX,BetaY,DeltaAngle\n");
    fprintf(fp,"CalibFator,0,0,0,0,0,1,1,0\n");
    */
    
    //calibration points
    //Table,No.,nType,nAlg,nFinished
    //PcbCalib,0,1,1,1
    fprintf(fp,"\nTable,No.,nType,nAlg,nFinished\n");
    fprintf(fp,"PcbCalib,0,1,1,0\n");
    
    //Table,No.,ID,offsetX,offsetY,Note,Model,Type,DevX,DevY
    //CalibPoint,0,0,2.12,92.34,Mark1,1903,1,0.04,0
    //CalibPoint,1,0,96.64,2.37,Mark2,1978,1,0,-0.06
    //CalibPoint,2,0,1.9,2.46,Mark3,1959,1,-0.06,0
    fprintf(fp,"\nTable,No.,ID,offsetX,offsetY,Note,Model,Type,DevX,DevY\n");
    sqlstring="SELECT deltaX,deltaY  from bom WHERE designator LIKE \"FID%\" ORDER BY deltaX ASC,deltaY DESC";
    rc=sqlite3_prepare_v2(glob_db,sqlstring, -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "Failed to fetch nozzle-data from phead: %s\n", sqlite3_errmsg(glob_db));
        return;
    }  
    rc = sqlite3_step (stmt);  
    if(rc == SQLITE_ROW){
        fprintf(fp,"CalibPoint,0,0,%s,%s,Mark1,0,1,0,0\n",
                sqlite3_column_text(stmt,0),
                sqlite3_column_text(stmt,1));
    } 
    sqlite3_finalize(stmt);
  
    //Mark2
    sqlstring="SELECT deltaX,deltaY  from bom WHERE designator LIKE \"FID%\" ORDER BY deltaX DESC,deltaY ASC";
    rc=sqlite3_prepare_v2(glob_db,sqlstring, -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "Failed to fetch nozzle-data from phead: %s\n", sqlite3_errmsg(glob_db));
        return;
    }  
   
    rc = sqlite3_step (stmt);  
    if(rc == SQLITE_ROW){
        fprintf(fp,"CalibPoint,1,0,%s,%s,Mark2,0,1,0,0\n",
                sqlite3_column_text(stmt,0),
                sqlite3_column_text(stmt,1));
    } 
 
    sqlite3_finalize(stmt);
      
    
    sqlstring="SELECT deltaX,deltaY  from bom WHERE designator LIKE \"FID%\" ORDER BY deltaX ASC,deltaY ASC";
    rc=sqlite3_prepare_v2(glob_db,sqlstring, -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "Failed to fetch nozzle-data from phead: %s\n", sqlite3_errmsg(glob_db));
        return;
    }  
    rc = sqlite3_step (stmt);  
    if(rc == SQLITE_ROW){
        fprintf(fp,"CalibPoint,2,0,%s,%s,Mark3,0,1,0,0\n",
                sqlite3_column_text(stmt,0),
                sqlite3_column_text(stmt,1));
    } 
    sqlite3_finalize(stmt);
    fflush(fp);
}

void dpv_placement_filename(char *filename){
    FILE *fp=fopen(filename, "w");
    dpv_placement_export(fp,filename);
    fclose(fp);
}


void dpv_placement_export(FILE *fp , char *chmt_filename){
     const char *sqlstring;
    
    //Header 
    fprintf(fp,"separated\n");
    fprintf(fp,"FILE,%s\n",chmt_filename);
    fprintf(fp,"PCBFILE,%s\n",chmt_filename);
    
    //https://www.tutorialspoint.com/c_standard_library/c_function_strftime.htm
    time_t timer;
    struct tm* tm_info;
    char day[3];
    char month[3];
    char year[5];
    char hour[3];
    char min[3];
    char sec[3];
    time(&timer);
    tm_info = localtime(&timer);
    strftime(day, 3, "%d", tm_info);
    strftime(month, 3, "%m", tm_info);
    strftime(year, 5, "%Y", tm_info);
    
    strftime(hour, 3, "%H", tm_info);
    strftime(min, 3, "%M", tm_info);
    strftime(sec, 3, "%S", tm_info);
     
    fprintf(fp,"DATE,%s/%s/%s\n", year,month,day);
    fprintf(fp,"TIME,%s:%s:%s\n", hour,min,sec);
    fprintf(fp,"SysVer,");
    fprintf(fp,"PANELYPE,0\n");
    
    //expot feeders
    dpv_export_feeders(fp);
    
    //panels are curently not supported
    
    fprintf(fp,"\nTable,No.,ID,DeltX,DeltY\n"
            "Panel_Coord,0,1,0,0\n");
    // extra string only: GROUP BY bom.value ,  bom.package
    sqlite3_stmt *stmt;
    sqlstring="SELECT bom.placement_order+1,phead.head_id,components.feeder_id,bom.deltaX,bom.deltaY,bom.angle,components.height,components.FeedRate,bom.skip,components.speed,bom.designator,components.package,components.value,components.status FROM bom LEFT JOIN components ON (bom.package=components.package AND bom.value=components.value)  LEFT JOIN phead ON (components.nozzle_id=phead.nozzle_id) WHERE designator NOT LIKE 'FID%'   GROUP BY bom.value,  bom.package ORDER BY bom.placement_order";
    int rc=sqlite3_prepare_v2(glob_db,sqlstring, -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(glob_db));
        return;
    }   

    fprintf(fp,"\nTable,No.,ID,PHead,STNo.,DeltX,DeltY,Angle,Height,Skip,Speed,Explain,Note,Delay\n");
    int countup=0;
    char *posx_char=malloc(100);
    char *posy_char=malloc(100);
    char *angle_char=malloc(100);
    char *height_char=malloc(100);
    do{
        rc = sqlite3_step (stmt);  
        if(rc != SQLITE_ROW) break;  

        snprintf(posx_char,100,"%g", sqlite3_column_double(stmt,3));
        dpv_replacechar(posx_char,',','.');
        snprintf(posy_char,100,"%g", sqlite3_column_double(stmt,4));
        dpv_replacechar(posy_char,',','.');
        snprintf(angle_char,100,"%g", dpv_adjust_angle(sqlite3_column_double(stmt,5)));
        dpv_replacechar(angle_char,',','.');
        snprintf(height_char,100,"%g", sqlite3_column_double(stmt,6));
        dpv_replacechar(height_char,',','.');
        //fprintf(fp,"value:%s\n",sqlite3_column_text(stmt,12));
    
        fprintf(fp,"EComponent,%i,%i,%i,%i,%s,%s,%s,%s,%i,%i,%s,%s|%s,0\n",
                countup++, //number
                sqlite3_column_int(stmt,0), //id,
                sqlite3_column_int(stmt,1)+1, //placement head
                sqlite3_column_int(stmt,2), //feeder
                posx_char, //deltaX
                posy_char, //deltaY
                angle_char, //angle
                height_char, //hight
                sqlite3_column_int(stmt,8) | sqlite3_column_int(stmt,13), //skip
                sqlite3_column_int(stmt,9), //speed
                sqlite3_column_text(stmt,10), //designator
                sqlite3_column_text(stmt,12), //
                sqlite3_column_text(stmt,11) //
                );
    }while(1);
    sqlite3_finalize(stmt);

    
    //here we put the nozzels
    //Table,No.,ID,NozzleType
    //Nozzle,0,1,2
    //Nozzle,1,2,2
    //Nozzle,2,3,2
    //Nozzle,3,4,2
    
    fprintf(fp,"\nTable,No.,ID,NozzleType\n");
    sqlstring="SELECT nozzle_id FROM phead ORDER BY head_id;";
    rc=sqlite3_prepare_v2(glob_db,sqlstring, -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "Failed to fetch nozzle-data from phead: %s\n", sqlite3_errmsg(glob_db));
        return;
    }  
    
    int nozzle_id_counter=0;
    do{
        rc = sqlite3_step (stmt);  
        if(rc != SQLITE_ROW) break;
        //Nozzle,2,3,2
        fprintf(fp,"Nozzle,%i,%i,%i\n",nozzle_id_counter,nozzle_id_counter+1,sqlite3_column_int(stmt,0));
        nozzle_id_counter=nozzle_id_counter+1;
        
    }while(1);
    for(;nozzle_id_counter<4;nozzle_id_counter++){
        fprintf(fp,"Nozzle,%i,%i,%i\n",nozzle_id_counter,nozzle_id_counter+1,0);
    }
    sqlite3_finalize(stmt);
    /*
    Table,No.,nType,nAlg,nFinished
    PcbCalib,0,0,0,0
    Table,No.,ID,offsetX,offsetY,Note
    CalibPoint,0,0,0,0,CalibPoint,1,0,0,0,
    Table,No.,DeltX,DeltY,AlphaX,AlphaY,BetaX,BetaY,DeltaAngle
    CalibFator,0,0,0,0,0,1,1,0 
    fprintf(fp,"\nTable,No.,nType,nAlg,nFinished\n");
    fprintf(fp,"PcbCalib,0,0,0,0\n");
    fprintf(fp,"\nTable,No.,ID,offsetX,offsetY,Note\n");
    fprintf(fp,"CalibPoint,0,0,0,0,CalibPoint,1,0,0,0,\n");
    fprintf(fp,"\nTable,No.,DeltX,DeltY,AlphaX,AlphaY,BetaX,BetaY,DeltaAngle\n");
    fprintf(fp,"CalibFator,0,0,0,0,0,1,1,0\n");
    */
    
    //calibration points
    //Table,No.,nType,nAlg,nFinished
    //PcbCalib,0,1,1,1
    fprintf(fp,"\nTable,No.,nType,nAlg,nFinished\n");
    fprintf(fp,"PcbCalib,0,1,1,0\n");
    
    //Table,No.,ID,offsetX,offsetY,Note,Model,Type,DevX,DevY
    //CalibPoint,0,0,2.12,92.34,Mark1,1903,1,0.04,0
    //CalibPoint,1,0,96.64,2.37,Mark2,1978,1,0,-0.06
    //CalibPoint,2,0,1.9,2.46,Mark3,1959,1,-0.06,0
    fprintf(fp,"\nTable,No.,ID,offsetX,offsetY,Note,Model,Type,DevX,DevY\n");
    sqlstring="SELECT deltaX,deltaY  from bom WHERE designator LIKE \"FID%\" ORDER BY deltaX ASC,deltaY DESC";
    rc=sqlite3_prepare_v2(glob_db,sqlstring, -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "Failed to fetch nozzle-data from phead: %s\n", sqlite3_errmsg(glob_db));
        return;
    }  
    rc = sqlite3_step (stmt);  
    if(rc == SQLITE_ROW){
        fprintf(fp,"CalibPoint,0,0,%s,%s,Mark1,0,1,0,0\n",
                sqlite3_column_text(stmt,0),
                sqlite3_column_text(stmt,1));
    } 
    sqlite3_finalize(stmt);
  
    //Mark2
    sqlstring="SELECT deltaX,deltaY  from bom WHERE designator LIKE \"FID%\" ORDER BY deltaX DESC,deltaY ASC";
    rc=sqlite3_prepare_v2(glob_db,sqlstring, -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "Failed to fetch nozzle-data from phead: %s\n", sqlite3_errmsg(glob_db));
        return;
    }  
   
    rc = sqlite3_step (stmt);  
    if(rc == SQLITE_ROW){
        fprintf(fp,"CalibPoint,1,0,%s,%s,Mark2,0,1,0,0\n",
                sqlite3_column_text(stmt,0),
                sqlite3_column_text(stmt,1));
    } 
 
    sqlite3_finalize(stmt);
      
    
    sqlstring="SELECT deltaX,deltaY  from bom WHERE designator LIKE \"FID%\" ORDER BY deltaX ASC,deltaY ASC";
    rc=sqlite3_prepare_v2(glob_db,sqlstring, -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "Failed to fetch nozzle-data from phead: %s\n", sqlite3_errmsg(glob_db));
        return;
    }  
    rc = sqlite3_step (stmt);  
    if(rc == SQLITE_ROW){
        fprintf(fp,"CalibPoint,2,0,%s,%s,Mark3,0,1,0,0\n",
                sqlite3_column_text(stmt,0),
                sqlite3_column_text(stmt,1));
    } 
    sqlite3_finalize(stmt);
    fflush(fp);
   
}