/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   dpv.h
 * Author: michael
 *
 * Created on September 26, 2018, 9:32 PM
 */

#ifndef DPV_H
#define DPV_H

#ifdef __cplusplus
extern "C" {
#endif
    

void dpv_placement_filename(char *filename);
void dpv_export_stdout(void);
void dpv_export_filename_test(void);
void dpv_export_filename(char *filename);
void dpv_export(FILE *fp , char *chmt_filename);
void dpv_placement_export(FILE *fp , char *chmt_filename);
int dpv_get_numbers_feeders_unassigned(void);
void dpv_export_feeders(FILE *fp);

#ifdef __cplusplus
}
#endif

#endif /* DPV_H */

