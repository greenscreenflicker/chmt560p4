/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "unified.h"

void feederlist_export_filename(char *filename){
    FILE *fp=fopen(filename, "w");
    feederlist_create_fp(fp);
    fclose(fp);
}

void feederlist_create_fp(FILE *fp){
    const char *sqlstring;
    sqlite3_stmt *stmt;
    int rc;
    
    fprintf(fp,"%-7s%-25s%-25s\n", "Feeder", "Value", "Footprint"); 
    
    sqlstring="SELECT feeder_id,value,package,status FROM components ORDER BY feeder_id";
    rc=sqlite3_prepare_v2(glob_db,sqlstring, -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "feederlist_create_fp: Failed to fetch data: %s\n", sqlite3_errmsg(glob_db));
        return;
    }
    int first_time_dnp=0;
    do{
        char feeder[1000];
        char value[1000];
        char footprint[1000];
        int status=0;
        
        rc = sqlite3_step (stmt);  
        if(rc != SQLITE_ROW) break;
               //Station,0,  1, 0,  0, 4,AD2-C0805,0.5,0,4,0,0,0,0
        
        strcpy(&feeder[0],sqlite3_column_text(stmt,0));
        helper_limit_string_len(feeder,6);
        strcpy(&value[0],sqlite3_column_text(stmt,1));
        helper_limit_string_len(value,24);
        strcpy(&footprint[0],sqlite3_column_text(stmt,2));
        helper_limit_string_len(footprint,24);
        status=sqlite3_column_int(stmt,3);
        if(first_time_dnp==0 && (status%2)>0){
            
            fprintf(fp,"===============================================================\n");
            fprintf(fp,"The following components are not placed and for reference only.\n");
            fprintf(fp,"===============================================================\n");
            first_time_dnp=1;
        }
        fprintf(fp,"%-7s%-25s%-25s\n",
                feeder, 
                value, 
                footprint); 
    }while(1);
    sqlite3_finalize(stmt);
 
}
