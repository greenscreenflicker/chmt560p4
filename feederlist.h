/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   feederlist.h
 * Author: michael
 *
 * Created on October 14, 2018, 8:05 PM
 */

#ifndef FEEDERLIST_H
#define FEEDERLIST_H

#ifdef __cplusplus
extern "C" {
#endif

void feederlist_export_filename(char *filename);
void feederlist_create_fp(FILE *fp);

#ifdef __cplusplus
}
#endif

#endif /* FEEDERLIST_H */

