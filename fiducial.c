/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//implements fiducal tab

#include "unified.h"

GtkWidget   *fiducial_grid;
GtkWidget   *fiducial_mark_entry[3][2];
GtkWidget   *fiducial_mark_label[3][2];

/*CREATE TABLE `fiducial` (
	`fiducial_id`	INTEGER,
	`offset_x`	NUMERIC,
	`offset_y`	NUMERIC
);*/


void fiducial_tab(void){
    fiducial_db_create();
    
    fiducial_grid=gtk_grid_new();
    gtk_grid_set_row_spacing(GTK_GRID(fiducial_grid),5);
    gtk_grid_set_column_spacing(GTK_GRID(fiducial_grid),5);
    
    //Mark1
    fiducial_mark_label[0][0]=gtk_label_new("Mark1 X");
    gtk_grid_attach(GTK_GRID(fiducial_grid),fiducial_mark_label[0][0],0,0,1,1);
    fiducial_mark_entry[0][0]=gtk_entry_new();
    g_signal_connect(fiducial_mark_entry[0][0],"activate",G_CALLBACK (fiducal_entry_callback),GINT_TO_POINTER(0));
    
    gtk_grid_attach(GTK_GRID(fiducial_grid),fiducial_mark_entry[0][0],1,0,1,1);
    fiducial_mark_label[0][1]=gtk_label_new("Mark1 Y");
    gtk_grid_attach(GTK_GRID(fiducial_grid),fiducial_mark_label[0][1],0,1,1,1);
    fiducial_mark_entry[0][1]=gtk_entry_new();
    gtk_grid_attach(GTK_GRID(fiducial_grid),fiducial_mark_entry[0][1],1,1,1,1);
      g_signal_connect(fiducial_mark_entry[0][1],"activate",G_CALLBACK (fiducal_entry_callback),GINT_TO_POINTER(1));
    
    //Mark2
    fiducial_mark_label[1][0]=gtk_label_new("Mark2 X");
    gtk_grid_attach(GTK_GRID(fiducial_grid),fiducial_mark_label[1][0],4,3,1,1);
    fiducial_mark_entry[1][0]=gtk_entry_new();
    gtk_grid_attach(GTK_GRID(fiducial_grid),fiducial_mark_entry[1][0],5,3,1,1);
    g_signal_connect(fiducial_mark_entry[1][0],"activate",G_CALLBACK (fiducal_entry_callback),GINT_TO_POINTER(2));
    fiducial_mark_label[1][1]=gtk_label_new("Mark2 Y");
    gtk_grid_attach(GTK_GRID(fiducial_grid),fiducial_mark_label[1][1],4,4,1,1);
    fiducial_mark_entry[1][1]=gtk_entry_new();
    gtk_grid_attach(GTK_GRID(fiducial_grid),fiducial_mark_entry[1][1],5,4,1,1);
    g_signal_connect(fiducial_mark_entry[1][1],"activate",G_CALLBACK (fiducal_entry_callback),GINT_TO_POINTER(3));
      
    //Mark3
    fiducial_mark_label[2][0]=gtk_label_new("Mark3 X");
    gtk_grid_attach(GTK_GRID(fiducial_grid),fiducial_mark_label[2][0],0,3,1,1);
    fiducial_mark_entry[2][0]=gtk_entry_new();
    gtk_grid_attach(GTK_GRID(fiducial_grid),fiducial_mark_entry[2][0],1,3,1,1);
      g_signal_connect(fiducial_mark_entry[2][0],"activate",G_CALLBACK (fiducal_entry_callback),GINT_TO_POINTER(4));
    fiducial_mark_label[2][1]=gtk_label_new("Mark3 Y");
    gtk_grid_attach(GTK_GRID(fiducial_grid),fiducial_mark_label[2][1],0,4,1,1);
    fiducial_mark_entry[2][1]=gtk_entry_new();
    gtk_grid_attach(GTK_GRID(fiducial_grid),fiducial_mark_entry[2][1],1,4,1,1);
    g_signal_connect(fiducial_mark_entry[2][1],"activate",G_CALLBACK (fiducal_entry_callback),GINT_TO_POINTER(5));
}

static void fiducal_entry_callback( GtkWidget *widget,
                            GtkWidget *entry )
{
  const gchar *entry_text;
  int id=GPOINTER_TO_INT(entry);
  int xy=id%2;
  int element=(id-xy)/2;
  entry_text= gtk_entry_get_text (GTK_ENTRY (fiducial_mark_entry[element][xy]));
  printf ("Entry changed: xy=%i-fid=%i text=%s\n", xy, element,entry_text);
  double location=atof(entry_text);
  free((void *)entry_text);
  
  //update the database
  char *singlestring=malloc(10000);
  if(xy==0){
      //we have x
        
        snprintf(singlestring,10000,
                 "UPDATE fiducial SET offset_x='%lf' WHERE fiducial_id=%i",
                location,
                element);
  }else{
        snprintf(singlestring,10000,
                 "UPDATE fiducial SET offset_y='%lf' WHERE fiducial_id=%i",
                location,
                element);
  }
  sqlite3_stmt *stmt;
  int sql_result= sqlite3_prepare_v2(glob_db, singlestring, -1, &stmt, NULL);
  if (sql_result != SQLITE_OK) {
      printf("Update on fiducial position failed.%s\n\r",sqlite3_errstr(sql_result)) ;
      return;
  }
  sqlite3_finalize(stmt);
  free(singlestring);
}

void fiducial_db_create(void){
    if (sqlite_check_if_table_exists("fiducial")) {
        sqlite3_stmt *stmt;
        const char sql_fiducial_create[]="CREATE TABLE `fiducial` (`fiducial_id`	INTEGER,`offset_x` NUMERIC,`offset_y` NUMERIC);";
        int sql_result= sqlite3_prepare_v2(glob_db, sql_fiducial_create, -1, &stmt, NULL);   
        if (sql_result != SQLITE_OK) {
            printf("Failed to create database%s\n\r",sqlite3_errstr(sql_result)) ;
            return;
        }
        sqlite3_finalize(stmt);
        
        char *singlestring=malloc(10000);
        snprintf(singlestring,10000,
                 "INSERT INTO fiducial "
                "(fiducial_id,feeder_id,offset_x,value,offset_y)"
                "VALUES('%i',\"%lf\",\"%lf\");",
                    0,
                    0.0,
                    0.0);
          
        sql_result=sqlite3_prepare_v2(glob_db, singlestring, -1, &stmt, NULL); 
        sqlite3_finalize(stmt);
        
        snprintf(singlestring,10000,
                 "INSERT INTO fiducial "
                "(fiducial_id,feeder_id,offset_x,value,offset_y)"
                "VALUES('%i',\"%lf\",\"%lf\");",
                    1,
                    0.0,
                    0.0);
          
        sql_result=sqlite3_prepare_v2(glob_db, singlestring, -1, &stmt, NULL); 
        sqlite3_finalize(stmt);
        
                
        snprintf(singlestring,10000,
                 "INSERT INTO fiducial "
                "(fiducial_id,feeder_id,offset_x,value,offset_y)"
                "VALUES('%i',\"%lf\",\"%lf\");",
                    2,
                    0.0,
                    0.0);
          
        sql_result=sqlite3_prepare_v2(glob_db, singlestring, -1, &stmt, NULL); 
        sqlite3_finalize(stmt);
        
        free(singlestring);
    
          
    }

}