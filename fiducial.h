/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   fiducial.h
 * Author: mmh
 *
 * Created on March 2, 2020, 5:44 PM
 */

#ifndef FIDUCIAL_H
#define FIDUCIAL_H

extern GtkWidget  *fiducial_grid;

void fiducial_tab(void);
void fiducial_db_create(void);
static void fiducal_entry_callback( GtkWidget *widget,
                            GtkWidget *entry );

#endif /* FIDUCIAL_H */

