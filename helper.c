/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "unified.h"

void helper_remove_extension_from_filename(char *filename){
    char *pointer;
    pointer=rindex(filename, '.');
   
    *(pointer)=0;
    // printf("newstringname:'%s'",filename);
}

void helper_get_extension_from_filename(char *filename){
    char *pointer_dot;
    int strlength=strlen(filename);
    pointer_dot=rindex(filename, '.');
    char *buffer=malloc(strlength);
    //we may not opperate on the same buffer, so we need to copy twice.
    int length=strlength-(pointer_dot-filename)-1;
    //printf("length: %i", length);
    //fflush(stdout);
    memcpy(buffer,pointer_dot+1,length);
    memcpy(filename,buffer,length);
    filename[length]=0; //terminator null ensured
    free(buffer);
}


void helper_filename_from_filepath(char *filepath, char* filename){
    char *pointer;
    pointer=rindex(filepath, '/')+1;
    //printf("before:'%s'-after'%s'",filepath,pointer);
    strcpy(filename,pointer);
}

void helper_limit_string_len(char *str, int maxlen){
    //if(strlen(str)>maxlen){
    str[maxlen]=0;
    //}
}

gboolean helper_component_status_decode_skip(int status){
    if(status & 0b1)return TRUE;
    return FALSE;
}

gboolean helper_component_status_decode_nothrow(int status){
    if(status & 0b10)return TRUE;
    return 0;
}

gboolean helper_component_status_decode_usevision(int status){
    if(status & 0b100)return TRUE;
    return 0;
}

gboolean helper_component_status_decode_seperate_mount(int status){
    if(status & 0b1000)return TRUE;
    return 0;
}

unsigned int helper_component_status_encode(gboolean skip, gboolean nothrow, gboolean vision, gboolean seperate){
    int status=(skip)+(nothrow<<1)+(vision<<2)+(seperate<<3);
    return status;
}