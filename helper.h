/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   helper.h
 * Author: michael
 *
 * Created on October 14, 2018, 1:05 PM
 */

#ifndef HELPER_H
#define HELPER_H

#include <glib-2.0/glib/gtypes.h>


#ifdef __cplusplus
extern "C" {
#endif


void helper_remove_extension_from_filename(char *filename);
void helper_filename_from_filepath(char *filepath, char* filename);
void helper_limit_string_len(char *str, int maxlen);
void helper_get_extension_from_filename(char *filename);

gboolean helper_component_status_decode_skip(int status);
gboolean helper_component_status_decode_nothrow(int status);
gboolean helper_component_status_decode_usevision(int status);
gboolean helper_component_status_decode_seperate_mount(int status);
unsigned int helper_component_status_encode(gboolean skip, gboolean nothrow, gboolean vision, gboolean seperate);

#ifdef __cplusplus
}
#endif

#endif /* HELPER_H */

