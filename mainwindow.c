/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "unified.h"

GtkWidget *mainwindow;
GtkWidget *mainwindow_grid;
GtkWidget *mainwindow_notebook;

GtkWidget *mainwindow_componentgrid;

GtkWidget *mainwindow_loadbutton;
GtkWidget *mainwindow_create_components;
GtkWidget *mainwindow_exportbutton;
GtkWidget *mainwindow_quitbutton;

GtkWidget    *mainwindow_bom_grid;
GtkWidget    *mainwindow_bom_vscroll;
GtkWidget    *mainwindow_bom_treeview;
GtkTreeModel *mainwindow_bom_treeview_model=NULL;
GtkWidget    *mainwindow_bom_frame;
GtkWidget    *mainwindow_bom_frame_grid;
//GtkWidget    *mainwindow_bom_button_components;
GtkWidget    *mainwindow_bom_button_optimize;
GtkWidget    *mainwindow_bom_checkbox_hide_dnp;
int           mainwindow_bom_hide_dnp_comp_flag=0;

GtkWidget   *mainwindow_phead_grid;
GtkWidget   *mainwindow_phead_grid_button_frame;
GtkWidget   *mainwindow_phead_grid_button_frame_grid;
GtkWidget   *mainwindow_phead_grid_button_add;
GtkWidget   *mainwindow_phead_grid_button_assign_head;
GtkWidget   *mainwindow_phead_grid_button_assign_similar;
GtkWidget   *mainwindow_phead_grid_button_forceupdate;
GtkWidget   *mainwindow_phead_grid_button_delete;
GtkWidget   *mainwindow_phead_vscroll;
GtkWidget   *mainwindow_phead_treeview;
GtkTreeModel *mainwindow_phead_model=NULL;



sqlite3 *glob_db;


char global_projectname[200];

static void mainwindow_destroy( GtkWidget *widget, gpointer data ){
    sqlite3_close(glob_db);
    gtk_main_quit ();
}

/*
void mainwindow_bonm_callback_skip(GtkWidget *widget, gpointer   data ){
    int status=gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget));
    printf("toggle callback: %i id=%i\n",status,GPOINTER_TO_UINT(data));
    fflush(stdout);
}


void mainwindow_bom(void){
    mainwindow_bom_treeview=gtk_grid_new ();
    
    char *partnumber=malloc(sizeof(char)*200);
    char *partnumber_old=malloc(sizeof(char)*200);
    char *footprint=malloc(sizeof(char)*200);
    char *footprint_old=malloc(sizeof(char)*200);
    char *headline=malloc(sizeof(char)*200);
    
    GtkWidget *mainwindow_bom_expander;
    GtkWidget *mainwindow_bom_expander_inside;
    int expanders=0;
    int expanders_inside=0;
  
    strcpy(partnumber_old,"");
    strcpy(footprint_old,"");
    
        
    qlite3_stmt *stmt;                                                                        
    int sql_result= sqlite3_prepare_v2(glob_db, "SELECT id,deltaX,deltaY,angle,skip,designator,value,package,side FROM bom Order by Value, package, designator, id", -1, &stmt, NULL);     
    if (sql_result != SQLITE_OK) {
        printf("Failed to prepare database %s\n\r",sqlite3_errstr(sql_result)) ;
        sqlite3_close(glob_db) ;
        return;
    }
    int dataelements=10000;
    
    mainwindow_bom_widgetarray=malloc(dataelements*sizeof(struct mainwindow_bom_widgets));
    unsigned int mainwindow_bom_widgetarray_elements=0;
    
    do {
        sql_result = sqlite3_step (stmt) ;
        if (sql_result == SQLITE_ROW) { 
            strcpy(partnumber,sqlite3_column_text(stmt,6));
            strcpy(footprint,sqlite3_column_text(stmt,7));   
            if(!
                    ((strcmp(partnumber,partnumber_old)==0) && 
                    (strcmp(footprint,footprint_old)==0))
                    ){
                strcpy(partnumber_old,partnumber);
                strcpy(footprint_old,footprint);
                snprintf(headline,100,"%s - %s",partnumber,footprint);
                expanders_inside=0;
                
                mainwindow_bom_expander=gtk_expander_new (headline);
                
                mainwindow_bom_expander_inside=gtk_grid_new();
                //Headlines
                gtk_grid_attach(GTK_GRID(mainwindow_bom_expander_inside),gtk_label_new("Designator"),0,expanders_inside,1,1);
                gtk_grid_attach(GTK_GRID(mainwindow_bom_expander_inside),gtk_label_new("X Position"),1,expanders_inside,1,1);
                gtk_grid_attach(GTK_GRID(mainwindow_bom_expander_inside),gtk_label_new("Y Position"),2,expanders_inside,1,1);
                gtk_grid_attach(GTK_GRID(mainwindow_bom_expander_inside),gtk_label_new("Orientation"),3,expanders_inside,1,1);
                gtk_grid_attach(GTK_GRID(mainwindow_bom_expander_inside),gtk_label_new("Side"),4,expanders_inside,1,1);
                gtk_grid_attach(GTK_GRID(mainwindow_bom_expander_inside),gtk_label_new("Skip"),5,expanders_inside,1,1);
                expanders_inside++;
                gtk_container_add(GTK_CONTAINER(mainwindow_bom_expander),mainwindow_bom_expander_inside);
                gtk_grid_attach(GTK_GRID(mainwindow_bom_treeview),mainwindow_bom_expander,0,expanders++,1,1);
            }
            //Designator
            mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].designator_widget=gtk_entry_new ();
            gtk_entry_set_text(GTK_ENTRY(mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].designator_widget),sqlite3_column_text(stmt,5));
            gtk_grid_attach(GTK_GRID(mainwindow_bom_expander_inside),mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].designator_widget,0,expanders_inside,1,1);
            // XPos
            mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].posx_widget=gtk_entry_new ();
            gtk_entry_set_text(GTK_ENTRY(mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].posx_widget),sqlite3_column_text(stmt,1));
            gtk_entry_set_alignment (GTK_ENTRY(mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].posx_widget),1);
            gtk_editable_set_editable(GTK_EDITABLE(mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].posx_widget), TRUE);
            gtk_grid_attach(GTK_GRID(mainwindow_bom_expander_inside),mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].posx_widget,1,expanders_inside,1,1);
            // YPos
            mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].posy_widget=gtk_entry_new();
            gtk_entry_set_text(GTK_ENTRY(mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].posy_widget),sqlite3_column_text(stmt,2));
            gtk_entry_set_alignment (GTK_ENTRY(mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].posy_widget),1);
            gtk_grid_attach(GTK_GRID(mainwindow_bom_expander_inside),mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].posy_widget,2,expanders_inside,1,1);
            // Orientation / Anlge
            mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].orientation_widget=gtk_entry_new();
            gtk_entry_set_text(GTK_ENTRY(mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].orientation_widget),sqlite3_column_text(stmt,3));
            gtk_entry_set_alignment (GTK_ENTRY(mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].orientation_widget),1);
            gtk_grid_attach(GTK_GRID(mainwindow_bom_expander_inside),mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].orientation_widget,3,expanders_inside,1,1);
            //side
            mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].skip_widget=gtk_label_new(sqlite3_column_text(stmt,8));
            gtk_grid_attach(GTK_GRID(mainwindow_bom_expander_inside),mainwindow_bom_widgetarray[mainwindow_bom_widgetarray_elements].skip_widget,4,expanders_inside,1,1);
            //place buton
            GtkWidget *skipbutton;
            skipbutton=gtk_check_button_new();
            gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(skipbutton), sqlite3_column_int(stmt,5));
            g_signal_connect (skipbutton, "toggled",  G_CALLBACK (mainwindow_bonm_callback_skip), GUINT_TO_POINTER(sqlite3_column_int(stmt,0)));
            gtk_grid_attach(GTK_GRID(mainwindow_bom_expander_inside),skipbutton,5,expanders_inside,5,1);
          
            expanders_inside++;
            mainwindow_bom_widgetarray_elements++;
        }else{
            break;
        }
    }while (1) ;
    //

    mainwindow_bom_widgetarray=realloc(mainwindow_bom_widgetarray,
            mainwindow_bom_widgetarray_elements*sizeof(struct mainwindow_bom_widgets));
           
    free(partnumber);
    free(partnumber_old);
    free(footprint);
    free(footprint_old);
    free(headline);
    free(mainwindow_bom_widgetarray);
    sqlite3_finalize(stmt);   

            

    

    
}
*/


enum
{
  bomview_id=0,
  bomview_deltaX,
  bomview_deltaY,
  bomview_angle,
  bomview_skip,
  bomview_designator,
  bomview_value,
  bomview_package,
  bomview_side,
  bomview_placement_order,
  bomview_NUM_COLS
} ;
/*

static GtkTreeModel *
create_and_fill_model (void)
{
  GtkListStore  *store;
  GtkTreeIter    iter;
  
  store = gtk_list_store_new (NUM_COLS, G_TYPE_STRING, G_TYPE_UINT);

  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter,
                      COL_NAME, "Heinz El-Mann",
                      COL_AGE, 51,
                      -1);

  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter,
                      COL_NAME, "Jane Doe",
                      COL_AGE, 23,
                      -1);

  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter,
                      COL_NAME, "Joe Bungop",
                      COL_AGE, 91,
                      -1);
  
  return GTK_TREE_MODEL (store);
}*/


static GtkTreeModel * sqlite_fill_bomlist (void){
    GtkTreeStore  *data;
    GtkTreeIter toplevel, child;
    data = gtk_tree_store_new (bomview_NUM_COLS, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, 
            G_TYPE_STRING, G_TYPE_STRING,G_TYPE_STRING,G_TYPE_STRING,G_TYPE_STRING,G_TYPE_STRING);
   
    
    sqlite3_stmt *stmt;   
    int sql_result;

    if(!mainwindow_bom_hide_dnp_comp_flag){
        sql_result= sqlite3_prepare_v2(glob_db, "SELECT id,deltaX,deltaY,angle,skip,designator,value,package,side,placement_order FROM bom Order by Value, package, designator, id", -1, &stmt, NULL);     
    }else{
        sql_result= sqlite3_prepare_v2(glob_db, 
                "SELECT id,bom.deltaX,bom.deltaY,angle,skip,designator,bom.value,bom.package,side,placement_order, (bom.skip%2+components.status%2) as dnp FROM bom LEFT JOIN components ON (bom.package=components.package AND bom.value=components.value) WHERE (bom.skip%2+components.status%2)<1 Order by bom.Value, bom.package, designator, id ",
                -1, &stmt, NULL);  
    }
    if (sql_result != SQLITE_OK) {
        printf("Failed to prepare database %s\n\r",sqlite3_errstr(sql_result)) ;
        //sqlite3_close(glob_db) ;
        return NULL;
    }
    char *partnumber=malloc(sizeof(char)*200);
    char *partnumber_old=malloc(sizeof(char)*200);
    char *footprint=malloc(sizeof(char)*200);
    char *footprint_old=malloc(sizeof(char)*200);
    
    do {
        sql_result = sqlite3_step (stmt) ;
        if (!(sql_result == SQLITE_ROW)) break;

        strcpy(partnumber,sqlite3_column_text(stmt,6));
        strcpy(footprint,sqlite3_column_text(stmt,7));   
        if(!
            ((strcmp(partnumber,partnumber_old)==0) && 
            (strcmp(footprint,footprint_old)==0))
            )
        {
            strcpy(partnumber_old,partnumber);
            strcpy(footprint_old,footprint);

            gtk_tree_store_append(data, &toplevel, NULL);
                gtk_tree_store_set (data, &toplevel,
                bomview_id, "-1",
                bomview_deltaX, "",
                bomview_deltaY, "",
                bomview_angle, "",
                bomview_skip, "",
                bomview_designator, "",
                bomview_value,partnumber,
                bomview_package, footprint,
                bomview_side, "",
                bomview_placement_order,"",
                -1);
        }
        gtk_tree_store_append(data, &child, &toplevel);
        gtk_tree_store_set (data, &child,
                bomview_id, sqlite3_column_text(stmt,0),
                bomview_deltaX,sqlite3_column_text(stmt,1),
                bomview_deltaY, sqlite3_column_text(stmt,2),
                bomview_angle, sqlite3_column_text(stmt,3),
                bomview_skip, sqlite3_column_text(stmt,4),
                bomview_designator, sqlite3_column_text(stmt,5),
                bomview_value,sqlite3_column_text(stmt,6),
                bomview_package, sqlite3_column_text(stmt,7),
                bomview_side, sqlite3_column_text(stmt,8),
                bomview_placement_order, sqlite3_column_text(stmt,9),
                -1);

    }while(1);
    sqlite3_finalize(stmt);
    /*
    gtk_tree_store_set (data, &toplevel,
        0, "A",
        1, "b",
        2, "c",
        3, "D",
        4, "E",
        5, "f",
        6, "g",
        7, "H",
        -1);
 
    
    gtk_tree_store_append(data, &child, &toplevel);
    gtk_tree_store_set (data, &child,
        0, "A1",
        1, "b2",
        2, "c3",
        3, "D4",
        4, "E5",
        5, "f6",
        6, "g7",
        7, "H8",
        -1);
*/
      /*
      char *zErrMsg;
      int rc = sqlite3_exec(glob_db, "Select * FROM bom Order by id", sqlite_callback_fill_bomlist, store, &zErrMsg);
      
    if( rc != SQLITE_OK ) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    } else {
        fprintf(stdout, "Operation done successfully\n");
    }
      */
      return GTK_TREE_MODEL (data);
}


void cell_edited_callback_bomview_deltaX (GtkCellRendererText *cell,
                                  gchar               *path_string,
                                  gchar               *new_text,
                                  gpointer             pointer){
    
    char *field=(char *)pointer;

    GtkTreeIter iter;
    gtk_tree_model_get_iter_from_string (mainwindow_bom_treeview_model,
                                    &iter,
                                    path_string);
    
    char *id;
    
    gtk_tree_model_get (GTK_TREE_MODEL(mainwindow_bom_treeview_model), &iter,
            bomview_id, &id,
            -1);

    
    if(strcmp(id,"-1")==0){
        int success=1;
        //we have a category the user wants to edit;
        printf("editing cathegories will be supported soon.\n");
        char *sqlstring=malloc(500);
        char *value;
        char *package;
        gtk_tree_model_get (GTK_TREE_MODEL(mainwindow_bom_treeview_model), &iter,
            bomview_value, &value,
            bomview_package, &package,
            -1);
        
        if(new_text[0]=='='){
            if((new_text[1]=='+') ||
                (new_text[1]=='-') ||
                (new_text[1]=='*') ||
                (new_text[1]=='/')){
                snprintf(sqlstring,500,"UPDATE bom SET `%s`=`%s`%c'%s' WHERE value='%s' AND package='%s'",
                    field,field,new_text[1],&new_text[2], value, package);
            }else{
                snprintf(sqlstring,500,"UPDATE bom SET `%s`='%s' WHERE value='%s' AND package='%s'",
                    field,&new_text[1], value, package);
            }
        }else{
            fprintf(stderr,"To update all, please put a '=' in front of your change.");
        }
        puts(sqlstring);
        sqlite3_stmt *stmt;
        int sql_result= sqlite3_prepare_v2(glob_db, sqlstring, -1, &stmt, NULL);     
        if (sql_result != SQLITE_OK) {
            printf("Failed to update database %s\n\r",sqlite3_errstr(sql_result)) ;
        }
        sqlite3_step (stmt); //execeute a step statement always after a update/insert to make it acvtive!
        sqlite3_finalize(stmt);
        if(success) mainwindow_bomview_full_update();
        free(sqlstring);
    }else{
        char *sqlstring=malloc(500);

        snprintf(sqlstring,500,"UPDATE bom SET `%s`='%s' WHERE id='%s';",
                field,new_text, id);
        printf("%s",sqlstring);
        sqlite3_stmt *stmt;
        int sql_result= sqlite3_prepare_v2(glob_db, sqlstring, -1, &stmt, NULL);     
        if (sql_result != SQLITE_OK) {
            printf("Failed to update database %s\n\r",sqlite3_errstr(sql_result)) ;
        }
        sqlite3_step (stmt); //execeute a step statement always after a update/insert to make it acvtive!
        sqlite3_finalize(stmt);

        snprintf(sqlstring,500,"SELECT %s FROM bom WHERE id='%s';",
                field,id);
        printf("%s",sqlstring);
        sql_result= sqlite3_prepare_v2(glob_db, sqlstring, -1, &stmt, NULL);     
        if (sql_result != SQLITE_OK) {
            printf("Failed to select value from database %s\n\r",sqlite3_errstr(sql_result)) ;
        }
        sql_result = sqlite3_step (stmt) ;
        if(sql_result == SQLITE_ROW){
            
            //  id,delayX,deltaY,angle,skip,designator,value,package,side
            if(strcmp(field,"deltaX")==0){
                gtk_tree_store_set (GTK_TREE_STORE(mainwindow_bom_treeview_model), &iter,
                        bomview_deltaX, sqlite3_column_text(stmt,0),
                    -1);
                printf("Update (Value%s) should be displayed.\n",sqlite3_column_text(stmt,0));
            }else if(strcmp(field,"deltaY")==0){
                gtk_tree_store_set (GTK_TREE_STORE(mainwindow_bom_treeview_model), &iter,
                        bomview_deltaY, sqlite3_column_text(stmt,0),
                    -1);
                printf("Update (Value%s) should be displayed.\n",sqlite3_column_text(stmt,0));  
             }else if(strcmp(field,"angle")==0){
                gtk_tree_store_set (GTK_TREE_STORE(mainwindow_bom_treeview_model), &iter,
                        bomview_angle, sqlite3_column_text(stmt,0),
                    -1);
                printf("Update (Value%s) should be displayed.\n",sqlite3_column_text(stmt,0));  
            }else if(strcmp(field,"skip")==0){
                gtk_tree_store_set (GTK_TREE_STORE(mainwindow_bom_treeview_model), &iter,
                        bomview_skip, sqlite3_column_text(stmt,0),
                    -1);
                printf("Update (Value%s) should be displayed.\n",sqlite3_column_text(stmt,0));  
            }else if(strcmp(field,"designator")==0){
                gtk_tree_store_set (GTK_TREE_STORE(mainwindow_bom_treeview_model), &iter,
                        bomview_designator, sqlite3_column_text(stmt,0),
                    -1);
                printf("Update (Value%s) should be displayed.\n",sqlite3_column_text(stmt,0));  
            }else if(strcmp(field,"value")==0){
                //gtk_tree_store_set (GTK_TREE_STORE(mainwindow_bom_treeview_model), &iter,bomview_value, sqlite3_column_text(stmt,0),-1);
                mainwindow_bomview_full_update();
               //printf("Update (Value%s) should be displayed.\n",sqlite3_column_text(stmt,0));  
            }else if(strcmp(field,"package")==0){
                //gtk_tree_store_set (GTK_TREE_STORE(mainwindow_bom_treeview_model), &iter,bomview_package, sqlite3_column_text(stmt,0),-1);
                mainwindow_bomview_full_update();
                //printf("Update (Value%s) should be displayed.\n",sqlite3_column_text(stmt,0));  
            }else if(strcmp(field,"side")==0){
                gtk_tree_store_set (GTK_TREE_STORE(mainwindow_bom_treeview_model), &iter,
                        bomview_side, sqlite3_column_text(stmt,0),
                    -1);
                printf("Update (Value%s) should be displayed.\n",sqlite3_column_text(stmt,0));  
            }else{
                printf("field unkown\n");
            }
            
        }else{
            printf("Unkown sql error.\n");
        }
        sqlite3_finalize(stmt);
        free(sqlstring);
    }
    fflush(stdout);
    g_free(id);
}

void mainwindow_bomview_full_update(void){
    gtk_tree_view_set_model (GTK_TREE_VIEW (mainwindow_bom_treeview), NULL);
    /* possible memory leak    
    if(mainwindow_bom_treeview_model){
        gtk_tree_store_clear(GTK_TREE_STORE(mainwindow_bom_treeview_model));
    }*/
    mainwindow_bom_treeview_model = sqlite_fill_bomlist ();
    gtk_tree_view_set_model (GTK_TREE_VIEW (mainwindow_bom_treeview), mainwindow_bom_treeview_model);
    g_object_unref(mainwindow_bom_treeview_model);
}
void mainwindow_bom(void){
    
    mainwindow_bom_grid=gtk_grid_new();
    
    
    mainwindow_bom_treeview=gtk_tree_view_new();
    
    GtkCellRenderer     *renderer;
    GtkTreeModel        *model;
    /*
    bomview_id, "",
    bomview_deltaX, "",
    bomview_deltaY, "",
    bomview_angle, "",
    bomview_skip, "",
    bomview_designator, "",
    bomview_value,partnumber,
    bomview_package, footprint,
    bomview_side, "",
*/
/*
    renderer = gtk_cell_renderer_text_new ();
    //g_object_set(renderer, "editable", TRUE, NULL);
    //g_signal_connect(renderer, "edited", (GCallback) cell_edited_callback_id, GUINT_TO_POINTER(bomview_id));
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_bom_treeview),
                                                 -1,      
                                                 "ID",  
                                                 renderer,
                                                 "text", bomview_id,
                                                 NULL);

*/
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) cell_edited_callback_bomview_deltaX, "value");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_bom_treeview),
                                                 -1,      
                                                 "Value",  
                                                 renderer,
                                                 "text", bomview_value,
                                                 NULL);   
    
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
     g_signal_connect(renderer, "edited", (GCallback) cell_edited_callback_bomview_deltaX, "package");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_bom_treeview),
                                                 -1,      
                                                 "Package",  
                                                 renderer,
                                                 "text", bomview_package,
                                                 NULL);   
    //deltaX,deltaY,angle,skip,designator,value,package,side
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) cell_edited_callback_bomview_deltaX, "deltaX");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_bom_treeview),
                                                 -1,      
                                                 "X",  
                                                 renderer,
                                                 "text", bomview_deltaX,
                                                 NULL);
     
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) cell_edited_callback_bomview_deltaX, "deltaY");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_bom_treeview),
                                                 -1,      
                                                 "Y",  
                                                 renderer,
                                                 "text", bomview_deltaY,
                                                 NULL);
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) cell_edited_callback_bomview_deltaX, "angle");         
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_bom_treeview),
                                                 -1,      
                                                 "angle",  
                                                 renderer,
                                                 "text", bomview_angle,
                                                 NULL);
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) cell_edited_callback_bomview_deltaX, "skip");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_bom_treeview),
                                                 -1,      
                                                 "skip",  
                                                 renderer,
                                                 "text", bomview_skip,
                                                 NULL); 
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) cell_edited_callback_bomview_deltaX, "designator");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_bom_treeview),
                                                 -1,      
                                                 "Designator",  
                                                 renderer,
                                                 "text", bomview_designator,
                                                 NULL);    
    
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", FALSE, NULL); //set non-editable
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_bom_treeview),
                                                 -1,      
                                                 "Order",  
                                                 renderer,
                                                 "text", bomview_placement_order,
                                                 NULL);    
    
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
     g_signal_connect(renderer, "edited", (GCallback) cell_edited_callback_bomview_deltaX, "side");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_bom_treeview),
                                                 -1,      
                                                 "Side",  
                                                 renderer,
                                                 "text",bomview_side,
                                                 NULL);   
    
    //printf("test\n");
    model = sqlite_fill_bomlist ();

    gtk_tree_view_set_model (GTK_TREE_VIEW (mainwindow_bom_treeview), model);
    mainwindow_bom_treeview_model=model;
    g_object_unref(model);

    mainwindow_bom_vscroll=gtk_scrolled_window_new (NULL, NULL);
    gtk_widget_set_hexpand (mainwindow_bom_vscroll,TRUE);
    gtk_widget_set_vexpand (mainwindow_bom_vscroll,TRUE);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (mainwindow_bom_vscroll),
                                GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
    
    gtk_container_add(GTK_CONTAINER(mainwindow_bom_vscroll),mainwindow_bom_treeview);
    gtk_grid_attach(GTK_GRID(mainwindow_bom_grid),mainwindow_bom_vscroll,0,0,1,1);
    
    //add now frame at the side
    mainwindow_bom_frame=gtk_frame_new("Tasks");
    gtk_grid_attach(GTK_GRID(mainwindow_bom_grid),mainwindow_bom_frame,1,0,1,1);
    mainwindow_bom_frame_grid=gtk_grid_new();
    gtk_container_add(GTK_CONTAINER(mainwindow_bom_frame),mainwindow_bom_frame_grid);
    
    int elements=0;
    
    mainwindow_bom_checkbox_hide_dnp = gtk_check_button_new_with_label ("Hide DNP-Components");
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mainwindow_bom_checkbox_hide_dnp),FALSE);
    gtk_grid_attach(GTK_GRID(mainwindow_bom_frame_grid),mainwindow_bom_checkbox_hide_dnp,0,elements++,1,1);
    g_signal_connect (GTK_TOGGLE_BUTTON (mainwindow_bom_checkbox_hide_dnp), "toggled", G_CALLBACK (mainwindow_bom_hidednp_callback), NULL);
    
    
    /*
    mainwindow_bom_button_components=gtk_button_new_with_label("Create Components");
    gtk_grid_attach(GTK_GRID(mainwindow_bom_frame_grid),mainwindow_bom_button_components,0,elements++,1,1);
     */
    mainwindow_bom_button_optimize=gtk_button_new_with_label("Optimize placement");
    g_signal_connect (mainwindow_bom_button_optimize, "clicked",G_CALLBACK (mainwindow_bom_action_optimize), NULL);  
    gtk_grid_attach(GTK_GRID(mainwindow_bom_frame_grid),mainwindow_bom_button_optimize,0,elements++,2,1);
    
} 

static void mainwindow_bom_hidednp_callback (GtkToggleButton *toggle_button, gpointer user_data){
    mainwindow_bom_hide_dnp_comp_flag=gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(mainwindow_bom_checkbox_hide_dnp));
    //printf("Remove me: DNP Toogle button clicked.");
    mainwindow_bomview_full_update();
    //force output
    //fflush(stdout);
    
}

void mainwindow_bom_action_optimize(void){
    optimalroute_perform();
    mainwindow_bomview_full_update();
}




void mainwindow_phead_cell_edit_callback (GtkCellRendererText *cell,
                                  gchar               *path_string,
                                  gchar               *new_text,
                                  gpointer             pointer){
    
    char *field=(char *)pointer;

    GtkTreeIter iter;
    gtk_tree_model_get_iter_from_string (mainwindow_phead_model,
                                    &iter,
                                    path_string);
    
    int id;
    
    gtk_tree_model_get (GTK_TREE_MODEL(mainwindow_phead_model), &iter,
            PHEAD_HEADID, &id,
            -1);
    
    //printf("phead: edited %s id %i to %s\n",field,id,new_text);


    char *sqlstring=malloc(500);

    snprintf(sqlstring,500,"UPDATE phead SET `%s`='%s' WHERE `head_id`='%i';",
        field,new_text, id);
    //printf("%s",sqlstring);
     
    sqlite3_stmt *stmt;
    int sql_result= sqlite3_prepare_v2(glob_db, sqlstring, -1, &stmt, NULL);     
    if (sql_result != SQLITE_OK) {
        printf("Failed to update database %s\n\r",sqlite3_errstr(sql_result)) ;
    }
    sqlite3_step (stmt); //execeute a step statement always after a update/insert to make it acvtive!
    sqlite3_finalize(stmt);
    
    //update view
    mainwindow_phead_full_update();
    free(sqlstring);
}


void mainwindow_phead_delete(void){
    GtkTreeSelection *mainwindow_phead_treeview_section;
    mainwindow_phead_treeview_section=gtk_tree_view_get_selection (GTK_TREE_VIEW(mainwindow_phead_treeview));
    GtkTreeIter iter;
    GtkTreeModel *model;

    if (gtk_tree_selection_get_selected (mainwindow_phead_treeview_section, &model, &iter))
    {
        int head_id;
        gtk_tree_model_get (model, &iter, PHEAD_HEADID, &head_id, -1);
        if(head_id>0){
            g_print ("You selected a head id %i\n", head_id);
            char sqldelete[300];
            snprintf(sqldelete,300,"DELETE FROM phead WHERE `head_id`='%i'",head_id);
            puts(sqldelete);
            char *zErrMsg;
            int rc = sqlite3_exec(glob_db,sqldelete, NULL, NULL, &zErrMsg);
            if( rc != SQLITE_OK ) {
                fprintf(stderr, "SQL error: %s\n", zErrMsg);
                sqlite3_free(zErrMsg);
            }else {
                //fprintf(stdout, "Operation done successfully\n");
                mainwindow_phead_full_update();
            }
        }
        fprintf(stderr,"no update due to invalid head_id\n");
    }else{
        fprintf(stderr, "no section\n");
    }
    fflush(stdout);
}

static GtkTreeModel * sqlite_phead_fill (void){
    GtkTreeStore  *data;
    GtkTreeIter toplevel, child;
    data = gtk_tree_store_new (PHEAD_NUMCOLS, G_TYPE_INT, G_TYPE_INT,G_TYPE_STRING);
   
    sqlite3_stmt *stmt;        
    int sql_result= sqlite3_prepare_v2(glob_db, "SELECT head_id,phead.nozzle_id,description,package FROM phead LEFT JOIN components ON (phead.nozzle_id=components.nozzle_id) GROUP BY head_id,package ORDER BY head_id", -1, &stmt, NULL);     
    if (sql_result != SQLITE_OK) {
        printf("Failed to prepare database %s\n\r",sqlite3_errstr(sql_result)) ;
    }
    
    int nozzleid_old=-100;
    int nozzleid_new;
    do {
        sql_result = sqlite3_step (stmt) ;
        if (!(sql_result == SQLITE_ROW)) break;
        nozzleid_new=sqlite3_column_int(stmt,0);
        if(nozzleid_new==nozzleid_old){
            gtk_tree_store_append(data, &child, &toplevel);
            gtk_tree_store_set (data, &child,
                    PHEAD_HEADID,-100,
                    PHEAD_NOZZLEID,sqlite3_column_int(stmt,1),
                    PHEAD_DESCRIPTION, sqlite3_column_text(stmt,3),
                    -1);              
        }else{
            gtk_tree_store_append(data, &toplevel, NULL);
            gtk_tree_store_set (data, &toplevel,
                    PHEAD_HEADID,sqlite3_column_int(stmt,0),
                    PHEAD_NOZZLEID,sqlite3_column_int(stmt,1),
                    PHEAD_DESCRIPTION, sqlite3_column_text(stmt,2),
                    -1);  
            if(sqlite3_column_type(stmt,3)!=SQLITE_NULL){
                gtk_tree_store_append(data, &child, &toplevel);
                gtk_tree_store_set (data, &child,
                        PHEAD_HEADID,-100,
                        PHEAD_NOZZLEID,sqlite3_column_int(stmt,1),
                        PHEAD_DESCRIPTION, sqlite3_column_text(stmt,3),
                        -1);  
            }
        }
        nozzleid_old=nozzleid_new;
    }while(1);

    sqlite3_finalize(stmt);
    return GTK_TREE_MODEL (data);
}

void mainwindow_phead_add_head(void){
    //printf("head\n");
    sqlite_phead_add_empty_row();
    mainwindow_phead_full_update();
}

void mainwindow_phead_full_update(void){
    gtk_tree_view_set_model (GTK_TREE_VIEW (mainwindow_phead_treeview), NULL);
    mainwindow_phead_model = sqlite_phead_fill();
    gtk_tree_view_set_model (GTK_TREE_VIEW (mainwindow_phead_treeview), mainwindow_phead_model);
    g_object_unref(mainwindow_phead_model);
}

gboolean mainwindow_phead_full_update_source(char * a){
    mainwindow_phead_full_update();
    return G_SOURCE_REMOVE;	
}

void mainwindow_phead(void){
    sqlite_phead_create_db_ifnotexists();
    //creates window for placement heads
    mainwindow_phead_grid=gtk_grid_new();
    mainwindow_phead_grid_button_frame=gtk_frame_new("Tasks");
    gtk_grid_attach(GTK_GRID(mainwindow_phead_grid),mainwindow_phead_grid_button_frame,1,0,1,1);
    
    int horizontal=0;
    
    mainwindow_phead_grid_button_frame_grid=gtk_grid_new();
    gtk_container_add(GTK_CONTAINER(mainwindow_phead_grid_button_frame),mainwindow_phead_grid_button_frame_grid);
    
    mainwindow_phead_grid_button_add=gtk_button_new_with_label("Add");
    g_signal_connect (mainwindow_phead_grid_button_add, "clicked",G_CALLBACK (mainwindow_phead_add_head), NULL);
    gtk_grid_attach(GTK_GRID(mainwindow_phead_grid_button_frame_grid),mainwindow_phead_grid_button_add,0,horizontal++,1,1);
    //assign component button
    mainwindow_phead_grid_button_assign_head=gtk_button_new_with_label("Assign component");
    g_signal_connect (mainwindow_phead_grid_button_assign_head, "clicked",G_CALLBACK (mainwindow_component_asign_head), NULL);    
    gtk_grid_attach(GTK_GRID(mainwindow_phead_grid_button_frame_grid),mainwindow_phead_grid_button_assign_head,0,horizontal++,1,1);
    //assign similar
    mainwindow_phead_grid_button_assign_similar=gtk_button_new_with_label("Assign similar");
    g_signal_connect (mainwindow_phead_grid_button_assign_similar, "clicked",G_CALLBACK (mainwindow_component_assign_similar), NULL);    
    gtk_grid_attach(GTK_GRID(mainwindow_phead_grid_button_frame_grid),mainwindow_phead_grid_button_assign_similar,0,horizontal++,1,1);
    //force update button
    mainwindow_phead_grid_button_forceupdate=gtk_button_new_with_label("Update List");
    g_signal_connect (mainwindow_phead_grid_button_forceupdate, "clicked",G_CALLBACK (mainwindow_phead_full_update), NULL);    
    gtk_grid_attach(GTK_GRID(mainwindow_phead_grid_button_frame_grid),mainwindow_phead_grid_button_forceupdate,0,horizontal++,1,1);
    //delete button
    mainwindow_phead_grid_button_delete=gtk_button_new_with_label("Delete");
    g_signal_connect (mainwindow_phead_grid_button_delete, "clicked",G_CALLBACK (mainwindow_phead_delete), NULL);    
    gtk_grid_attach(GTK_GRID(mainwindow_phead_grid_button_frame_grid),mainwindow_phead_grid_button_delete,0,horizontal++,1,1);
    
    mainwindow_phead_treeview=gtk_tree_view_new();

    
    //to display
    GtkCellRenderer     *renderer;
    GtkTreeModel        *model;


    
    //value
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) mainwindow_phead_cell_edit_callback, "nozzle_id");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_phead_treeview),
                                             -1,      
                                             "Name",  
                                             renderer,
                                             "text", PHEAD_NOZZLEID,
                                             NULL);
    renderer = gtk_cell_renderer_text_new ();    
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) mainwindow_phead_cell_edit_callback, "description");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_phead_treeview),
                                             -1,      
                                             "Description/Footprint",  
                                             renderer,
                                             "text", PHEAD_DESCRIPTION,
                                             NULL);    
    mainwindow_phead_model=sqlite_phead_fill();
    gtk_tree_view_set_model (GTK_TREE_VIEW (mainwindow_phead_treeview), mainwindow_phead_model);
    g_object_unref(mainwindow_phead_model);
    
    mainwindow_phead_vscroll=gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (mainwindow_phead_vscroll),
                                GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
    gtk_container_add(GTK_CONTAINER(mainwindow_phead_vscroll),mainwindow_phead_treeview);
    gtk_grid_attach(GTK_GRID(mainwindow_phead_grid),mainwindow_phead_vscroll,0,0,1,1);
    gtk_widget_set_hexpand (mainwindow_phead_vscroll,TRUE);
    gtk_widget_set_vexpand (mainwindow_phead_vscroll,TRUE);
    
}
void mainwindow_export_placement_check_dpv(void){
        GtkWidget *dialog;
    GtkFileChooser *chooser;
    GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
    gint res;

    int unassigned=(dpv_get_numbers_feeders_unassigned());
    
    if(unassigned!=0){
        
        GtkWidget  *dialog;
        GtkDialogFlags flags = GTK_DIALOG_DESTROY_WITH_PARENT;
        dialog = gtk_message_dialog_new (GTK_WINDOW(mainwindow),
                                         flags,
                                         GTK_MESSAGE_ERROR,
                                         GTK_BUTTONS_CLOSE,
                                         "Assign components to feeders: %i missing.",
                                         unassigned);
        gtk_window_set_title(GTK_WINDOW(dialog), "Assign components");
        gtk_dialog_run (GTK_DIALOG (dialog));
        gtk_widget_destroy (dialog);
        return;
        
    }
    
    dialog = gtk_file_chooser_dialog_new ("Save DPV File",
                                      GTK_WINDOW(mainwindow),
                                      action,
                                      ("_Cancel"),
                                      GTK_RESPONSE_CANCEL,
                                      ("_Save"),
                                      GTK_RESPONSE_ACCEPT,
                                      NULL);
    chooser = GTK_FILE_CHOOSER (dialog);

    gtk_file_chooser_set_do_overwrite_confirmation (chooser, TRUE);

    char proposefilename[200];
    char filename_withoutpath[200];
    snprintf(proposefilename,200,"%s-placement.dpv",global_projectname);
    //printf("poposed filname:%s",proposefilename);
    gtk_file_chooser_set_filename (chooser,proposefilename);
    helper_filename_from_filepath(proposefilename,filename_withoutpath);
    gtk_file_chooser_set_current_name (chooser,filename_withoutpath);
    //Filter for easier selction
    GtkFileFilter *filter;
    filter = gtk_file_filter_new();
    gtk_file_filter_add_pattern(filter,"*.dpv");
    gtk_file_filter_set_name(filter,"DPV (Charmhigh placement file)");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);
    
    res = gtk_dialog_run (GTK_DIALOG (dialog));
    if (res == GTK_RESPONSE_ACCEPT){
        char *filename;
        filename = gtk_file_chooser_get_filename (chooser);
        dpv_placement_filename(filename);
        g_free (filename);
    }

    gtk_widget_destroy (dialog);
}
void mainwindow_export_dpv(void){
    GtkWidget *dialog;
    GtkFileChooser *chooser;
    GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
    gint res;

    int unassigned=(dpv_get_numbers_feeders_unassigned());
    
    if(unassigned!=0){
        
        GtkWidget  *dialog;
        GtkDialogFlags flags = GTK_DIALOG_DESTROY_WITH_PARENT;
        dialog = gtk_message_dialog_new (GTK_WINDOW(mainwindow),
                                         flags,
                                         GTK_MESSAGE_ERROR,
                                         GTK_BUTTONS_CLOSE,
                                         "Assign components to feeders: %i missing.",
                                         unassigned);
        gtk_window_set_title(GTK_WINDOW(dialog), "Assign components");
        gtk_dialog_run (GTK_DIALOG (dialog));
        gtk_widget_destroy (dialog);
        return;
        
    }
    
    dialog = gtk_file_chooser_dialog_new ("Save DPV File",
                                      GTK_WINDOW(mainwindow),
                                      action,
                                      ("_Cancel"),
                                      GTK_RESPONSE_CANCEL,
                                      ("_Save"),
                                      GTK_RESPONSE_ACCEPT,
                                      NULL);
    chooser = GTK_FILE_CHOOSER (dialog);

    gtk_file_chooser_set_do_overwrite_confirmation (chooser, TRUE);

    char proposefilename[200];
    char filename_withoutpath[200];
    snprintf(proposefilename,200,"%s.dpv",global_projectname);
    //printf("poposed filname:%s",proposefilename);
    gtk_file_chooser_set_filename (chooser,proposefilename);
    helper_filename_from_filepath(proposefilename,filename_withoutpath);
    gtk_file_chooser_set_current_name (chooser,filename_withoutpath);
    //Filter for easier selction
    GtkFileFilter *filter;
    filter = gtk_file_filter_new();
    gtk_file_filter_add_pattern(filter,"*.dpv");
    gtk_file_filter_set_name(filter,"DPV (Charmhigh placement file)");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);
    
    res = gtk_dialog_run (GTK_DIALOG (dialog));
    if (res == GTK_RESPONSE_ACCEPT){
        char *filename;
        filename = gtk_file_chooser_get_filename (chooser);
        dpv_export_filename(filename);
        g_free (filename);
    }

    gtk_widget_destroy (dialog);

}

void mainwindow_import_pos_filechooser(void){
    GtkWidget *dialog;
    GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;
    gint res;

    dialog = gtk_file_chooser_dialog_new ("Open pos File",
                                      GTK_WINDOW(mainwindow),
                                      action,
                                      ("_Cancel"),
                                      GTK_RESPONSE_CANCEL,
                                      ("_Open"),
                                      GTK_RESPONSE_ACCEPT,
                                      NULL);
    GtkFileFilter *filter;
    filter = gtk_file_filter_new();
    gtk_file_filter_add_pattern(filter,"*.sqlite");
    gtk_file_filter_set_name(filter,"SQLITE (Placement Database)");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);
    filter = gtk_file_filter_new();
    gtk_file_filter_add_pattern(filter,"*.pos");
    gtk_file_filter_set_name(filter,"POS (KiCAD posfile)");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);
        
    res = gtk_dialog_run (GTK_DIALOG (dialog));
    if (res == GTK_RESPONSE_ACCEPT)
    {
        char *filename;
        GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
        filename = gtk_file_chooser_get_filename (chooser);
        strcpy(global_projectname,filename); //copy project name
        
        char *ending=malloc(strlen(global_projectname)+1);
        strcpy(ending,global_projectname); //copy project name
        
        //check what ending we have
        helper_get_extension_from_filename(ending);
        //generate database
        char *sqldatabasename=malloc(200);
        helper_remove_extension_from_filename(global_projectname);
        snprintf(sqldatabasename,200,"%s.sqlite", global_projectname);
        if(strcmp("pos",ending)==0){
            //create databases and co
            sqlite_open_database(sqldatabasename);
            sqlite_read_posfile(filename);
        }else if(strcmp("sqlite",ending)==0){
            sqlite_open_database(sqldatabasename);
        }else{
            fprintf(stderr,"This file type is not supported (%s)\n",ending);
            exit(1);
        }
        
        free(ending);
        
        //Update Window title
        char *filname_only=malloc(200);
        helper_filename_from_filepath(global_projectname,filname_only);
        snprintf(sqldatabasename,200,"CHMT Placement %s", filname_only);
        gtk_window_set_title(GTK_WINDOW(mainwindow),sqldatabasename);
        g_free (filename);
        free(sqldatabasename);
        free(filname_only);
    } else if (res != GTK_RESPONSE_ACCEPT){
        printf("No source file was selected. Terminating.\n");
        exit(1);
    }else{
        printf("Eror selecting source file. Terminating.\n");
        exit(1);
    }
    gtk_widget_destroy (dialog);
}

void mainwindow_export_feederlist(void){
    GtkWidget *dialog;
    GtkFileChooser *chooser;
    GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
    gint res;

    dialog = gtk_file_chooser_dialog_new ("Export Feeder List",
                                      GTK_WINDOW(mainwindow),
                                      action,
                                      ("_Cancel"),
                                      GTK_RESPONSE_CANCEL,
                                      ("_Save"),
                                      GTK_RESPONSE_ACCEPT,
                                      NULL);
    chooser = GTK_FILE_CHOOSER (dialog);

    gtk_file_chooser_set_do_overwrite_confirmation (chooser, TRUE);

    char proposefilename[200];
    char filename_withoutpath[200];
    snprintf(proposefilename,200,"%s_feederlist.txt",global_projectname);
    //printf("poposed filname:%s",proposefilename);
    gtk_file_chooser_set_filename (chooser,proposefilename);
    helper_filename_from_filepath(proposefilename,filename_withoutpath);
    gtk_file_chooser_set_current_name (chooser,filename_withoutpath);
    //Filter for easier selction
    GtkFileFilter *filter;
    filter = gtk_file_filter_new();
    gtk_file_filter_add_pattern(filter,"*.txt");
    gtk_file_filter_set_name(filter,"TXT (Human Readable Text Document)");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);
    
    res = gtk_dialog_run (GTK_DIALOG (dialog));
    if (res == GTK_RESPONSE_ACCEPT){
        char *filename;
        filename = gtk_file_chooser_get_filename (chooser);
        feederlist_export_filename(filename);
        g_free (filename);
    }

    gtk_widget_destroy (dialog);

}



void mainwindow_inject_call(GSourceFunc func, gpointer data){
        GMainContext *gtk_context_var;
	GSource *source;
	source = g_idle_source_new();
	g_source_set_callback(source, func, data, NULL);
	g_source_attach(source, gtk_context_var);
	g_source_unref(source);
}

void inject_timeout(GSourceFunc func, gpointer data, guint ms){
	GSource *source;
        GMainContext *gtk_context_var;
	source = g_timeout_source_new (ms);
	g_source_set_callback(source, func, data, NULL);
	g_source_attach(source,gtk_context_var);
	g_source_unref(source);
}

void mainwindow_display(void){
    mainwindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    //open chooser dialog for posfile
    mainwindow_import_pos_filechooser();
    /* When the window is given the "delete-event" signal (this is given
     * by the window manager, usually by the "close" option, or on the
     * titlebar), we ask it to call the delete_event () function
     * as defined above. The data passed to the callback
     * function is NULL and is ignored in the callback function. */
    g_signal_connect (mainwindow, "delete-event", G_CALLBACK (mainwindow_destroy), NULL);
    
    gtk_window_set_position(GTK_WINDOW(mainwindow), GTK_WIN_POS_CENTER_ALWAYS);
    mainwindow_grid=gtk_grid_new();

   
    //Create notebook
    mainwindow_notebook = gtk_notebook_new ();
    gtk_widget_set_hexpand (mainwindow_notebook,TRUE);
    gtk_widget_set_vexpand (mainwindow_notebook,TRUE);
    
    gtk_notebook_set_tab_pos (GTK_NOTEBOOK (mainwindow_notebook), GTK_POS_TOP);  
    gtk_grid_attach(GTK_GRID(mainwindow_grid),mainwindow_notebook,0,0,4,1);
    



    
   
    mainwindow_bom();

    gtk_notebook_append_page( GTK_NOTEBOOK(mainwindow_notebook),
                           mainwindow_bom_grid,
                           gtk_label_new("BOM"));
  
    mainwindow_component_gui();
    gtk_notebook_append_page( GTK_NOTEBOOK(mainwindow_notebook),
                               mainwindow_components_grid,
                               gtk_label_new("Components"));
  
    mainwindow_phead();
    gtk_notebook_append_page( GTK_NOTEBOOK(mainwindow_notebook),
                               mainwindow_phead_grid,
                               gtk_label_new("Heads"));
    
    fiducial_tab();
    gtk_notebook_append_page( GTK_NOTEBOOK(mainwindow_notebook),
                           fiducial_grid,
                           gtk_label_new("Fiducial"));
    
    
    gtk_container_add (GTK_CONTAINER (mainwindow), mainwindow_grid);
    
    //buttons
    /*
    mainwindow_loadbutton=gtk_button_new_with_label("Load");
    g_signal_connect (mainwindow_loadbutton, "clicked",G_CALLBACK (optimalroute_test), NULL);
    gtk_grid_attach(GTK_GRID(mainwindow_grid),mainwindow_loadbutton,0,1,1,1);

    mainwindow_create_components=gtk_button_new_with_label("Create Components");
    g_signal_connect (mainwindow_create_components, "clicked",G_CALLBACK (mainwindow_button_create_components), NULL);
    gtk_grid_attach(GTK_GRID(mainwindow_grid),mainwindow_create_components,1,1,1,1);
    */
    mainwindow_exportbutton=gtk_button_new_with_label("Export");
    g_signal_connect (mainwindow_exportbutton, "clicked",G_CALLBACK (mainwindow_export_dpv), NULL);
    gtk_grid_attach(GTK_GRID(mainwindow_grid),mainwindow_exportbutton,2,1,1,1);
    mainwindow_quitbutton=gtk_button_new_with_label("Quit");
    g_signal_connect (mainwindow_quitbutton, "clicked",G_CALLBACK (mainwindow_destroy), NULL);
    gtk_grid_attach(GTK_GRID(mainwindow_grid),mainwindow_quitbutton,3,1,1,1);
    
    gtk_window_set_default_size(GTK_WINDOW(mainwindow),
            gdk_screen_get_width(gdk_screen_get_default()),
            gdk_screen_get_height(gdk_screen_get_default()));
    
    gtk_widget_show_all(mainwindow);
    
   
}