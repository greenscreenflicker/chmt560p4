/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   mainwindow.h
 * Author: michael
 *
 * Created on September 15, 2018, 1:55 PM
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#ifdef __cplusplus
extern "C" {
#endif


void mainwindow_display(void);

void mainwindow_bomview_full_update(void);
static GtkTreeModel * sqlite_component_treeview_fill (void);
void mainwindow_phead_full_update(void);
gboolean mainwindow_phead_full_update_source(char *a);
void mainwindow_phead_delete(void);
void mainwindow_component_asign_head(void);
void mainwindow_component_general_reload(void);
void mainwindow_component_assign_similar(void);
void mainwindow_export_dpv(void);
void mainwindow_export_placement_check_dpv(void);
void mainwindow_import_pos_filechooser(void);
void mainwindow_export_feederlist(void);
static void mainwindow_button_create_components( GtkWidget *widget, gpointer data );
void mainwindow_bom_action_optimize(void);

//mainwindow bom
static void mainwindow_bom_hidednp_callback (GtkToggleButton *toggle_button, gpointer user_data);
extern int mainwindow_bom_hide_dnp_comp_flag;

//phead
extern GtkWidget   *mainwindow_phead_treeview;
extern GtkWidget   *mainwindow;

//Static Variable
extern sqlite3 *glob_db;
extern char global_projectname[200];

//feeder_name, deltaX, deltaY,FeedRate,height,speed,status,sizeX,sizeY,heightTake
enum
{
    COMPONENT_GUI_ID=0,
    COMPONENT_GUI_FEEDER,
    COMPONENT_GUI_FEEDERWIDTH,
    COMPONENT_GUI_VALUE,
    COMPONENT_GUI_PACKAGE,
    COMPONENT_GUI_DELAYX,
    COMPONENT_GUI_DELAYY,
    COMPONENT_GUI_DELTAROT,  
    COMPONENT_GUI_FEEDRATE,
    COMPONENT_GUI_HEIGHT,
    COMPONENT_GUI_SPEED,
    COMPONENT_GUI_STATUS_SKIP,
    COMPONENT_GUI_STATUS_NOTHROW,
    COMPONENT_GUI_STATUS_USEVISION,
    COMPONENT_GUI_STATUS_SEPERATE_MOUNT,
    COMPONENT_GUI_SIZEX,
    COMPONENT_GUI_SIZEY,
    COMPONENT_GUI_HEIGHTTAKE,
    COMPONENT_GUI_DELAYTAKE,
    COMPONENT_GUI_NOZZLE_ID, 
    COMPONENT_GUI_NUMCOLS
} ;

enum
{
    PHEAD_HEADID=0,
    PHEAD_NOZZLEID=1,
    PHEAD_DESCRIPTION=2,
    PHEAD_NUMCOLS=3
} ;

#ifdef __cplusplus
}
#endif

#endif /* MAINWINDOW_H */

