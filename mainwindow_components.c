/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "unified.h"

GtkWidget    *mainwindow_components_grid;
GtkWidget    *mainwindow_components_frame;
GtkWidget    *mainwindow_components_framegrid;
GtkWidget    *mainwindow_components_button_create_comp;
GtkWidget    *mainwindow_components_button_assign;
GtkWidget    *mainwindow_components_button_assign_similar;
GtkWidget    *mainwindow_components_button_feederlist;
GtkWidget    *mainwindow_components_button_placement_check;
GtkWidget    *mainwindow_components_button_backimport_positions;
GtkWidget    *mainwindow_components_vscroll;
GtkWidget    *mainwindow_components_treeview;
GtkTreeModel *mainwindow_components_model=NULL;

static GtkTreeModel * sqlite_component_treeview_fill (void){
    GtkTreeStore  *data;
    GtkTreeIter toplevel, child;
    data = gtk_tree_store_new (COMPONENT_GUI_NUMCOLS,
            G_TYPE_INT,     //COMPONENT_GUI_ID
            G_TYPE_INT,     //COMPONENT_GUI_FEEDER
            G_TYPE_INT,     //COMPONENT_GUI_FEEDERWIDTH
            G_TYPE_STRING,  //COMPONENT_GUI_VALUE
            G_TYPE_STRING,  //COMPONENT_GUI_PACKAGE
            G_TYPE_STRING,  //COMPONENT_GUI_DELAYX
            G_TYPE_STRING,  //COMPONENT_GUI_DELAYY
            G_TYPE_STRING,  //COMPONENT_GUI_DELTAROT
            G_TYPE_STRING,  //COMPONENT_GUI_FEEDRATE
            G_TYPE_STRING,  //COMPONENT_GUI_HEIGHT
            G_TYPE_STRING,  //COMPONENT_GUI_SPEED
            G_TYPE_BOOLEAN, //COMPONENT_GUI_STATUS_SKIP
            G_TYPE_BOOLEAN, //COMPONENT_GUI_STATUS_NOTHROW
            G_TYPE_BOOLEAN, //COMPONENT_GUI_STATUS_USEVISION
            G_TYPE_BOOLEAN, //COMPONENT_GUI_STATUS_SEPERATE_MOUNT
            G_TYPE_STRING,  //COMPONENT_GUI_SIZEX
            G_TYPE_STRING,  //COMPONENT_GUI_SIZEY
            G_TYPE_STRING,  //COMPONENT_GUI_HEIGHTTAKE
            G_TYPE_STRING,  //COMPONENT_GUI_DELAYTAKE
            G_TYPE_STRING  //COMPONENT_GUI_NOZZLE_ID
            );
   
    sqlite3_stmt *stmt;        
    int sql_result= sqlite3_prepare_v2(glob_db, "SELECT comp_id,feeder_id,value,package,deltaX,deltaY,FeedRate,height,speed,status,sizeX,sizeY,heighttake,nozzle_id,delayTake,feeder_width,deltaRot FROM components  Order by feeder_id,value, package;", -1, &stmt, NULL);     
    if (sql_result != SQLITE_OK) {
        printf("Failed to prepare database %s\n\r",sqlite3_errstr(sql_result)) ;
        //sqlite3_close(glob_db) ;
        return GTK_TREE_MODEL (data);
    }
    
    do {
        sql_result = sqlite3_step (stmt) ;
        if (!(sql_result == SQLITE_ROW)) break;
        int status=sqlite3_column_int(stmt,9);
        /*printf("status %i:%i,%i,%i,%i\n",
                status,
                helper_component_status_decode_nothrow(status),
                helper_component_status_decode_seperate_mount(status),
                helper_component_status_decode_skip(status),
                helper_component_status_decode_usevision(status));
       */ 
        gtk_tree_store_append(data, &toplevel, NULL); 
        gtk_tree_store_set (data, &toplevel,
                COMPONENT_GUI_ID,sqlite3_column_int(stmt,0),
                COMPONENT_GUI_FEEDER,sqlite3_column_int(stmt,1),
                COMPONENT_GUI_FEEDERWIDTH,sqlite3_column_int(stmt,15),
                COMPONENT_GUI_VALUE, sqlite3_column_text(stmt,2),
                COMPONENT_GUI_PACKAGE, sqlite3_column_text(stmt,3),
                COMPONENT_GUI_DELAYX, sqlite3_column_text(stmt,4),
                COMPONENT_GUI_DELAYY, sqlite3_column_text(stmt,5),
                COMPONENT_GUI_DELTAROT, sqlite3_column_text(stmt,16),
                COMPONENT_GUI_FEEDRATE, sqlite3_column_text(stmt,6),
                COMPONENT_GUI_HEIGHT, sqlite3_column_text(stmt,7),
                COMPONENT_GUI_SPEED, sqlite3_column_text(stmt,8),
                COMPONENT_GUI_SIZEX, sqlite3_column_text(stmt,10),
                COMPONENT_GUI_SIZEY, sqlite3_column_text(stmt,11),
                COMPONENT_GUI_HEIGHTTAKE, sqlite3_column_text(stmt,12),
                COMPONENT_GUI_NOZZLE_ID, sqlite3_column_text(stmt,13),
                COMPONENT_GUI_DELAYTAKE, sqlite3_column_text(stmt,14),
                COMPONENT_GUI_STATUS_NOTHROW, helper_component_status_decode_nothrow(status),
                COMPONENT_GUI_STATUS_SEPERATE_MOUNT, helper_component_status_decode_seperate_mount(status),
                COMPONENT_GUI_STATUS_SKIP, helper_component_status_decode_skip(status),
                COMPONENT_GUI_STATUS_USEVISION, helper_component_status_decode_usevision(status),
                -1);  
    }while(1);
    sqlite3_finalize(stmt);
    
    return GTK_TREE_MODEL (data);
}

void mainwindow_component_general_reload(void){
    gtk_tree_view_set_model (GTK_TREE_VIEW (mainwindow_components_treeview), NULL);
    mainwindow_components_model = sqlite_component_treeview_fill();
    gtk_tree_view_set_model (GTK_TREE_VIEW (mainwindow_components_treeview), mainwindow_components_model);
    g_object_unref(mainwindow_components_model);
}
void mainwindow_components_cell_toggled_callback (GtkCellRendererToggle *cell,
                                   gchar                 *path_string,
                                   gpointer               user_data){
    GtkTreeIter iter;
    gtk_tree_model_get_iter_from_string (mainwindow_components_model,
                                    &iter,
                                    path_string);
    
    int toogled=GPOINTER_TO_INT(user_data);
    //printf("toggled: %i\n",toogled);
    int id;
    int nothrow,seperate,skip,vision;
    gtk_tree_model_get (GTK_TREE_MODEL(mainwindow_components_model), &iter,
            COMPONENT_GUI_ID,&id,
            COMPONENT_GUI_STATUS_NOTHROW, &nothrow,
            COMPONENT_GUI_STATUS_SEPERATE_MOUNT,&seperate,
            COMPONENT_GUI_STATUS_SKIP,&skip,
            COMPONENT_GUI_STATUS_USEVISION,&vision,
            -1);
    
    if(toogled==COMPONENT_GUI_STATUS_NOTHROW){
        nothrow=!nothrow;
    }else if(toogled==COMPONENT_GUI_STATUS_SEPERATE_MOUNT){
        seperate=!seperate;
    }else if(toogled==COMPONENT_GUI_STATUS_SKIP){
        skip=!skip;
    }else if(toogled==COMPONENT_GUI_STATUS_USEVISION){
        vision=!vision;
    }else{
        fprintf(stdout,"toggling error\n");
        return;
    }    
    unsigned int status=helper_component_status_encode(skip,nothrow,vision,seperate);
    
    char *sqlstr=malloc(500);
    sprintf(sqlstr,"UPDATE components SET status='%u' WHERE comp_id='%i'",status,id);
    
    char *err_msg;
    int sql_result=sqlite3_exec(glob_db, sqlstr, NULL, 0, &err_msg);
    if (sql_result != SQLITE_OK) {
        printf("Failed to insert in component database: %s\n\r",err_msg);
        printf("%s\n",sqlstr);
    }
    fflush(stdout);
    free(sqlstr);
    
    //did we update the skip, so we need to the move the feeder to the end.
    if(toogled==COMPONENT_GUI_STATUS_SKIP){
        sqlite_component_fix_feedernumbers();
    }
    //general update
    mainwindow_component_general_reload();
}

void mainwindow_components_cell_edit_callback (GtkCellRendererText *cell,
                                  gchar               *path_string,
                                  gchar               *new_text,
                                  gpointer             pointer){
    
    char *field=(char *)pointer;

    GtkTreeIter iter;
    gtk_tree_model_get_iter_from_string (mainwindow_components_model,
                                    &iter,
                                    path_string);
    
    int id;
    
    gtk_tree_model_get (GTK_TREE_MODEL(mainwindow_components_model), &iter,
            COMPONENT_GUI_ID, &id,
            -1);
    
    printf("edited %s id %i to %s\n",field,id,new_text);
   
    char *sqlstring=malloc(500);

    snprintf(sqlstring,500,"UPDATE components SET `%s`='%s' WHERE `comp_id`='%i';",
        field,new_text, id);
     printf("%s",sqlstring);
     
    sqlite3_stmt *stmt;
    int sql_result= sqlite3_prepare_v2(glob_db, sqlstring, -1, &stmt, NULL);     
    if (sql_result != SQLITE_OK) {
        printf("Failed to update database %s\n\r",sqlite3_errstr(sql_result)) ;
    }
    sqlite3_step (stmt); //execeute a step statement always after a update/insert to make it acvtive!
    sqlite3_finalize(stmt);

    //if feeder numbers have been edited, we need to fix the feeder numbers automatically
    if(strcmp(field,"feeder_width")==0){
        sqlite_component_fix_feedernumbers();
    }
    //update view
    mainwindow_component_general_reload();

    free(sqlstring);
}

//opens a file chooser to select a file to backimport
void mainwindow_components_backimport_positions_gui(void){
    GtkWidget *dialog;
    GtkFileChooser *chooser;
    GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
    
    dialog = gtk_file_chooser_dialog_new ("Backimport DPV: Positions",
                                      GTK_WINDOW(mainwindow),
                                      action,
                                      ("_Cancel"),
                                      GTK_RESPONSE_CANCEL,
                                      ("_Open"),
                                      GTK_RESPONSE_ACCEPT,
                                      NULL);
    chooser = GTK_FILE_CHOOSER (dialog);

    //gtk_file_chooser_set_do_overwrite_confirmation (chooser, TRUE);

    char proposefilename[200];
    char filename_withoutpath[200];
    snprintf(proposefilename,200,"%s-placement.dpv",global_projectname);
    gtk_file_chooser_set_filename (chooser,proposefilename);
    helper_filename_from_filepath(proposefilename,filename_withoutpath);
    gtk_file_chooser_set_current_name (chooser,filename_withoutpath);
    //Filter for easier selction
    GtkFileFilter *filter;
    filter = gtk_file_filter_new();
    gtk_file_filter_add_pattern(filter,"*.dpv");
    gtk_file_filter_set_name(filter,"DPV (Charmhigh placement file)");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);
    
    gint res = gtk_dialog_run (GTK_DIALOG (dialog));
    if (res == GTK_RESPONSE_ACCEPT){
        char *filename;
        filename = gtk_file_chooser_get_filename (chooser);
        mainwindow_component_backimport_position_worker(filename);
        g_free (filename);
    }

    gtk_widget_destroy (dialog);
}

void mainwindow_component_backimport_position_worker(char * filename){
    printf("Opening %s\n",filename);
    printf("implementation to be done!!\n");
    fflush(stdout);
}



void mainwindow_component_gui(void){
    sqlite_component_list_create_db_ifnotexists();
    
    mainwindow_components_grid=gtk_grid_new();
    mainwindow_components_frame=gtk_frame_new("Tasks");
    gtk_grid_attach(GTK_GRID(mainwindow_components_grid),mainwindow_components_frame,1,0,1,1);
    
    int horizontal=0;
    mainwindow_components_framegrid=gtk_grid_new();
    gtk_container_add(GTK_CONTAINER(mainwindow_components_frame),mainwindow_components_framegrid);
    
    //button create components
    mainwindow_components_button_create_comp=gtk_button_new_with_label("Create Component");
    g_signal_connect (mainwindow_components_button_create_comp, "clicked",G_CALLBACK (mainwindow_button_create_components), NULL);    
    gtk_grid_attach(GTK_GRID(mainwindow_components_framegrid),mainwindow_components_button_create_comp,0,horizontal++,1,1);
    //button assign
    mainwindow_components_button_assign=gtk_button_new_with_label("Assign Component");
    g_signal_connect (mainwindow_components_button_assign, "clicked",G_CALLBACK (mainwindow_component_asign_head), NULL);    
    gtk_grid_attach(GTK_GRID(mainwindow_components_framegrid),mainwindow_components_button_assign,0,horizontal++,1,1);
    //button assign similar
    mainwindow_components_button_assign_similar=gtk_button_new_with_label("Assign similar");
    g_signal_connect (mainwindow_components_button_assign_similar, "clicked",G_CALLBACK (mainwindow_component_assign_similar), NULL);    
    gtk_grid_attach(GTK_GRID(mainwindow_components_framegrid),mainwindow_components_button_assign_similar,0,horizontal++,1,1);
    //button export dpv_placement_check
    mainwindow_components_button_placement_check=gtk_button_new_with_label("Placement check DPV");
    g_signal_connect (mainwindow_components_button_placement_check, "clicked",G_CALLBACK (mainwindow_export_placement_check_dpv), NULL);    
    gtk_grid_attach(GTK_GRID(mainwindow_components_framegrid),mainwindow_components_button_placement_check,0,horizontal++,1,1);
    //button backimport 
    mainwindow_components_button_backimport_positions=gtk_button_new_with_label("Positions from DPV");
    g_signal_connect (mainwindow_components_button_backimport_positions, "clicked",G_CALLBACK (mainwindow_components_backimport_positions_gui), NULL);    
    gtk_grid_attach(GTK_GRID(mainwindow_components_framegrid),mainwindow_components_button_backimport_positions,0,horizontal++,1,1);
    //button feederlist export
    mainwindow_components_button_feederlist=gtk_button_new_with_label("Feeder List Export");
    g_signal_connect (mainwindow_components_button_feederlist, "clicked",G_CALLBACK (mainwindow_export_feederlist), NULL);    
    gtk_grid_attach(GTK_GRID(mainwindow_components_framegrid),mainwindow_components_button_feederlist,0,horizontal++,1,1);
    
    mainwindow_components_treeview=gtk_tree_view_new();
    //to display
    GtkCellRenderer     *renderer;
    GtkCellRenderer *toggle_renderer;
    GtkTreeModel        *model;
     
    //Feeder ID
    renderer = gtk_cell_renderer_text_new ();
    //g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) mainwindow_components_cell_edit_callback, "feeder_id");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "Feeder Number",  
                                                 renderer,
                                                 "text", COMPONENT_GUI_FEEDER,
                                                 NULL);    
    //value
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) mainwindow_components_cell_edit_callback, "feeder_width");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "Feeder Width",  
                                                 renderer,
                                                 "text", COMPONENT_GUI_FEEDERWIDTH,
                                                 NULL);    
    
    //value
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) mainwindow_components_cell_edit_callback, "value");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "Value",  
                                                 renderer,
                                                 "text", COMPONENT_GUI_VALUE,
                                                 NULL); 
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) mainwindow_components_cell_edit_callback, "package");
    //g_signal_connect(renderer, "edited", (GCallback) NULL, "value");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "Package",  
                                                 renderer,
                                                 "text", COMPONENT_GUI_PACKAGE,
                                                 NULL);   
    //deltaX
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) mainwindow_components_cell_edit_callback, "deltaX");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "deltaX",  
                                                 renderer,
                                                 "text", COMPONENT_GUI_DELAYX,
                                                 NULL);   
    //deltaX
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) mainwindow_components_cell_edit_callback, "deltaY");
    //g_signal_connect(renderer, "edited", (GCallback) NULL, "deltaY");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "deltaY",  
                                                 renderer,
                                                 "text", COMPONENT_GUI_DELAYY,
                                                 NULL);     
    
      //deltaRot
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) mainwindow_components_cell_edit_callback, "deltaRot");
    //g_signal_connect(renderer, "edited", (GCallback) NULL, "deltaY");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "deltaRot",  
                                                 renderer,
                                                 "text", COMPONENT_GUI_DELTAROT,
                                                 NULL);    
     //FeedRate
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    //g_signal_connect(renderer, "edited", (GCallback) NULL, "FeedRate");
    g_signal_connect(renderer, "edited", (GCallback) mainwindow_components_cell_edit_callback, "FeedRate");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "FeedRate",  
                                                 renderer,
                                                 "text", COMPONENT_GUI_FEEDRATE,
                                                 NULL);       
    //COMPONENT_GUI_HEIGHT
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) mainwindow_components_cell_edit_callback,  "height");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "height",  
                                                 renderer,
                                                 "text", COMPONENT_GUI_HEIGHT,
                                                 NULL);     
    //COMPONENT_GUI_SPEED
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) mainwindow_components_cell_edit_callback,  "speed");
    //g_signal_connect(renderer, "edited", (GCallback) NULL, "speed");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "speed",  
                                                 renderer,
                                                 "text", COMPONENT_GUI_SPEED,
                                                 NULL);   

      //COMPONENT_GUI_SIZEX
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) mainwindow_components_cell_edit_callback,  "sizeX");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "sizeX",  
                                                 renderer,
                                                 "text", COMPONENT_GUI_SIZEX,
                                                 NULL);      
      //COMPONENT_GUI_SIZEY
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) mainwindow_components_cell_edit_callback,  "sizeY");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "sizeY",  
                                                 renderer,
                                                 "text", COMPONENT_GUI_SIZEY,
                                                 NULL);       
      //COMPONENT_GUI_HEIGHTTAKE
    renderer = gtk_cell_renderer_text_new ();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited", (GCallback) mainwindow_components_cell_edit_callback, "heightTake");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "heightTake",  
                                                 renderer,
                                                 "text", COMPONENT_GUI_HEIGHTTAKE,
                                                 NULL);       
  
    toggle_renderer = gtk_cell_renderer_toggle_new ();
    gtk_cell_renderer_toggle_set_radio(GTK_CELL_RENDERER_TOGGLE(toggle_renderer),FALSE);
    gtk_cell_renderer_toggle_set_activatable(GTK_CELL_RENDERER_TOGGLE(toggle_renderer),TRUE);
    g_signal_connect(toggle_renderer, "toggled", G_CALLBACK(mainwindow_components_cell_toggled_callback),  GINT_TO_POINTER(COMPONENT_GUI_STATUS_SEPERATE_MOUNT));
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "solomount",  
                                                 toggle_renderer,
                                                 "active", COMPONENT_GUI_STATUS_SEPERATE_MOUNT,
                                                 NULL);   
    
    toggle_renderer = gtk_cell_renderer_toggle_new ();
    gtk_cell_renderer_toggle_set_radio(GTK_CELL_RENDERER_TOGGLE(toggle_renderer),FALSE);
    gtk_cell_renderer_toggle_set_activatable(GTK_CELL_RENDERER_TOGGLE(toggle_renderer),TRUE);
    g_signal_connect(toggle_renderer, "toggled", G_CALLBACK(mainwindow_components_cell_toggled_callback),  GINT_TO_POINTER(COMPONENT_GUI_STATUS_USEVISION));
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "Vision",  
                                                 toggle_renderer,
                                                 "active", COMPONENT_GUI_STATUS_USEVISION,
                                                 NULL);    
    
    toggle_renderer = gtk_cell_renderer_toggle_new ();
    gtk_cell_renderer_toggle_set_radio(GTK_CELL_RENDERER_TOGGLE(toggle_renderer),FALSE);
    gtk_cell_renderer_toggle_set_activatable(GTK_CELL_RENDERER_TOGGLE(toggle_renderer),TRUE);
    g_signal_connect(toggle_renderer, "toggled", G_CALLBACK(mainwindow_components_cell_toggled_callback),  GINT_TO_POINTER(COMPONENT_GUI_STATUS_NOTHROW));
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "NoThrow",  
                                                 toggle_renderer,
                                                 "active", COMPONENT_GUI_STATUS_NOTHROW,
                                                 NULL);
    
    toggle_renderer = gtk_cell_renderer_toggle_new ();
    gtk_cell_renderer_toggle_set_radio(GTK_CELL_RENDERER_TOGGLE(toggle_renderer),FALSE);
    gtk_cell_renderer_toggle_set_activatable(GTK_CELL_RENDERER_TOGGLE(toggle_renderer),TRUE);
    g_signal_connect(toggle_renderer, "toggled", G_CALLBACK(mainwindow_components_cell_toggled_callback),  GINT_TO_POINTER(COMPONENT_GUI_STATUS_SKIP));
    //g_signal_connect (cell, "toggled", G_CALLBACK (check), store);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "Skip",  
                                                 toggle_renderer,
                                                 //"toggle", COMPONENT_GUI_STATUS_SKIP,
                                                 "active", COMPONENT_GUI_STATUS_SKIP,
                                                 NULL);    
    
    renderer = gtk_cell_renderer_combo_new (); 
    //g_object_set(renderer, "editable", TRUE, NULL);
   //g_signal_connect(renderer, "edited", (GCallback) mainwindow_components_cell_edit_callback, "heightTake");
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (mainwindow_components_treeview),
                                                 -1,      
                                                 "Placement Head",  
                                                 renderer,
                                                 "text", COMPONENT_GUI_NOZZLE_ID,
                                                 NULL);       
    
    
    mainwindow_components_model=sqlite_component_treeview_fill();
    gtk_tree_view_set_model (GTK_TREE_VIEW (mainwindow_components_treeview), mainwindow_components_model);
    g_object_unref(mainwindow_components_model);
    
    //make multiple selectable
    GtkTreeSelection  *selection;
    selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainwindow_components_treeview));
    gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);
    
    //Finish
    mainwindow_components_vscroll=gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (mainwindow_components_vscroll),
                                GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
    
    gtk_container_add(GTK_CONTAINER(mainwindow_components_vscroll),mainwindow_components_treeview);
    gtk_widget_set_hexpand (mainwindow_components_vscroll,TRUE);
    gtk_widget_set_vexpand (mainwindow_components_vscroll,TRUE);
    gtk_grid_attach(GTK_GRID(mainwindow_components_grid),mainwindow_components_vscroll,0,0,1,1);
}

void mainwindow_component_asign_head_foreach(GtkTreeModel *model,
                                GtkTreePath *path,
                                GtkTreeIter *iter,
                                gpointer data){
    
    int component_id;
    gtk_tree_model_get (model, iter, COMPONENT_GUI_ID, &component_id, -1);
        
        
    //printf("update %i to %i\n",component_id,GPOINTER_TO_INT(data));
    char *sqlstring=malloc(500);

    snprintf(sqlstring,500,"UPDATE components SET `nozzle_id`='%i' WHERE `comp_id`='%i';",
        GPOINTER_TO_INT(data),component_id);
    //puts(sqlstring);
    //printf("%s",sqlstring);
     
    sqlite3_stmt *stmt;
    int sql_result= sqlite3_prepare_v2(glob_db, sqlstring, -1, &stmt, NULL);     
    if (sql_result != SQLITE_OK) {
        printf("Failed to update database %s\n\r",sqlite3_errstr(sql_result)) ;
    }
    sqlite3_step (stmt); //execeute a step statement always after a update/insert to make it acvtive!
    sqlite3_finalize(stmt);
    free(sqlstring);
}
//assign 
void mainwindow_component_assign_similar(void){
    sqlite_assign_similar_phead();
    mainwindow_component_general_reload();
    mainwindow_phead_full_update();
}

static void mainwindow_button_create_components( GtkWidget *widget, gpointer data ){
    sqlite_component_list_create_db();
    sqlite_component_list_fill();
    mainwindow_component_general_reload();
}

void mainwindow_component_asign_head(void){
    GtkTreeSelection *mainwindow_phead_treeview_section;
    mainwindow_phead_treeview_section=gtk_tree_view_get_selection (GTK_TREE_VIEW(mainwindow_phead_treeview));
    GtkTreeIter iter;
    GtkTreeModel *model;

    if (gtk_tree_selection_get_selected (mainwindow_phead_treeview_section, &model, &iter))
    {
        int nozzle_id;
        gtk_tree_model_get (model, &iter, PHEAD_NOZZLEID, &nozzle_id, -1);
        
        
        GtkTreeSelection *mainwindow_component_treeview_section;
        mainwindow_component_treeview_section=gtk_tree_view_get_selection (GTK_TREE_VIEW(mainwindow_components_treeview));
        gtk_tree_selection_selected_foreach 
                                        (mainwindow_component_treeview_section,
                                         G_CALLBACK( mainwindow_component_asign_head_foreach),
                                         GINT_TO_POINTER(nozzle_id));
        mainwindow_component_general_reload();
        mainwindow_phead_full_update();
    }else{
        fprintf(stderr,"no head selected\n");
    }

}
