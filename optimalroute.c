/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "unified.h"

double optimalroute_distance(optrte_t *a, optrte_t *b){
    double dX=(a->deltaX-b->deltaX);
    double dY=(a->deltaY-b->deltaY);
    return (dX*dX+dY*dY);
}

optrte_t* optimalroute_mindistance(optrte_t *base, optrte_t *b){
    if(base==NULL) return NULL;
    double bestdistance;
    double thisdistance;
    optrte_t* nearest;
    bestdistance=optimalroute_distance(base,b);
    nearest=base;
    while(((++base)->bom_id)>-1){
        if(base->pos>0) continue;
        thisdistance=optimalroute_distance(base,b);
        if(thisdistance<bestdistance){
            bestdistance=thisdistance;
            nearest=base;
        }
    }
    return nearest;
}

optrte_t* optimalroute_mindistance_head(optrte_t *base, optrte_t *b, int head){
    if(base==NULL) return NULL;
    double bestdistance;
    double thisdistance;
    optrte_t* nearest;
    bestdistance=1000.0*1000.0*1000.0*1000.0; //unrealistic high value
    nearest=NULL;
    while(((base)->bom_id)>-1){
        if( (base->pos>-1) ||
                (base->headid!=head)){
            base++; 
            continue;
        }
        thisdistance=optimalroute_distance(base,b);
        if(thisdistance<bestdistance){
            bestdistance=thisdistance;
            nearest=base;
        }
        base++;
    }
    return nearest;
}

//ToDo: Realloc
optrte_t* optimalroute_getdata(void){
    sqlite3_stmt *stmt;
    const char *sqlstring;
    sqlstring="SELECT bom.id,bom.deltaX,bom.deltaY,phead.head_id FROM bom LEFT JOIN components ON (bom.package=components.package AND bom.value=components.value)  LEFT JOIN phead ON (components.nozzle_id=phead.nozzle_id) WHERE designator NOT LIKE 'FID%' ORDER BY bom.id";
    int rc=sqlite3_prepare_v2(glob_db,sqlstring, -1, &stmt, NULL);
        if (rc != SQLITE_OK) {
        fprintf(stderr, "Optimalroute_getdata: Failed to fetch data: %s\n", sqlite3_errmsg(glob_db));
        return NULL;
    }   

    //
    
    optrte_t* base=malloc(sizeof(optrte_t)*10000);
    optrte_t* data=base;
    do{
        rc = sqlite3_step (stmt);  
        if(rc != SQLITE_ROW) break;
        data->bom_id=sqlite3_column_int(stmt,0);
        data->deltaX=sqlite3_column_double(stmt,1);
        data->deltaY=sqlite3_column_double(stmt,2);
        data->pos=-1;
        data->headid=sqlite3_column_int(stmt,3);
        //optimalroute_list_singledata(data);
        data++;
        
    }while(1);
    data->bom_id=-1; //mark terminator
    return base;
}

void optimalroute_list_singledata(optrte_t *base){
    if(!base){
        printf("empty pointer\n");
    }else{
        printf("id:%i,x=%g,y=%g,head=%i,placed=%i\n",
                base->bom_id,
                base->deltaX,
                base->deltaY,
                base->headid,
                base->pos
        );
    }
}

//Finds the center of all components and puts it to center
void optimalroute_find_center(optrte_t *base, optrte_t *center){
    int elements=0;
    center->deltaX=0;
    center->deltaY=0;
    center->bom_id=-1;
    center->pos=-1;
    while(((base)->bom_id)>-1){
        center->deltaX+=base->deltaX;
        center->deltaY+=base->deltaY;
        elements++;
        base++;
    }
    if(elements){
        center->deltaX=center->deltaX/elements;
        center->deltaY=center->deltaY/elements;
    }
}

int optimalroute_optimize_loop_terminate_condition(optrte_t **head_to_check,int elements){
    int terminate=0;
    int cnt;
    for(cnt=0;cnt<elements;cnt++){
        if(head_to_check[cnt]){
            terminate++;
        }
    }
    return terminate;
}

void optimalroute_optimize(optrte_t *base){
    optrte_t center;
    optimalroute_find_center(base,&center);
    printf("calculated center:\n");
    optimalroute_list_singledata(&center);
    
    /*
    //find starting vector
    optrte_t *start;
    start=optimalroute_mindistance_head(base,&zero,0);
    printf("starter:");
    optimalroute_list_singledata(start);
    */
    
    optrte_t *head[5];
    int i;
    int position=0;
    head[0]=&center;
    head[1]=&center;
    head[2]=&center;
    head[3]=&center;
    head[4]=&center;
    do{
        for(i=0;i<4;i++){
            head[i+1]=optimalroute_mindistance_head(base,head[i],i);
            if(head[i+1]){
               head[i+1]->pos=position++;
               optimalroute_list_singledata(head[i+1]);
            }
        }
    }while(optimalroute_optimize_loop_terminate_condition(&head[1],4));
}

void optimalroute_perform(void){
    optrte_t* base;
    base=optimalroute_getdata(); 
    if(!base) return;
    //optimalroute_list_singledata(pos);
    //printf("distance %lf",optimalroute_distance(pos,pos+1));
    optimalroute_optimize(base);
    sqlite_bom_set_placement_order(base);
}