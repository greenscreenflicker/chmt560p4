/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   optimalroute.h
 * Author: michael
 *
 * Created on October 24, 2018, 7:53 PM
 */

#ifndef OPTIMALROUTE_H
#define OPTIMALROUTE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct optimalroute_placement{
    int bom_id;
    double deltaX;
    double deltaY;
    int headid;
    int pos;
} optrte_t;

void optimalroute_perform(void);
void optimalroute_list_singledata(optrte_t *base);
void optimalroute_find_center(optrte_t *base, optrte_t *center);
int optimalroute_optimize_loop_terminate_condition(optrte_t **head_to_check,int elements);

#ifdef __cplusplus
}
#endif

#endif /* OPTIMALROUTE_H */

