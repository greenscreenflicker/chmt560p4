/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "unified.h"
#include "mainwindow.h"
static int sqllite_callback_test(void *data, int argc, char **argv, char **azColName){
   int i;
   for(i = 0; i<argc; i++){
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   
   printf("\n");
   return 0;
}


 
//returns the distance to a string
int distance_to_slash(char *data){
    int count=0;
    while(1){
        if((data[count]==' ')){
            return count;
        }else if((data[count]=='\0')){
            return -1;
        }else if((data[count]=='\n')){
            return count;
        }else{
            count++;
        }
    }
}

int distance_to_nonslash(char* data){
    int count=0;
    while(1){
        if((data[count]!=' ')){
            return count;
        }else if((data[count]=='\0')){
            return -1;
        }else if((data[count]=='\n')){
            return count;
        }else{
            count++;
        }
    }
}


int distance_to_newline_or_fileend(char* data){
    int count=0;
    while(1){
        if((data[count]=='\n')){
            return count;
        }else if((data[count]=='\0')){
            return count;
        }else{
            count++;
        }
    }
}


int sqlite_get_bomid(sqlite3 *db){
    sqlite3_stmt *res;
    int rc;
    rc = sqlite3_prepare_v2(db, "SELECT count(id),max(id)+1 FROM bom", -1, &res, 0);    
    if (rc != SQLITE_OK) {
        fprintf(stderr, "sqlite_get_bomid: Failed to fetch data: %s\n", sqlite3_errmsg(db));
        //sqlite3_close(db);
        
        return 1;
    }    
    
    rc = sqlite3_step(res);
    
    int nextid;
    int nextid2;
    
    if (rc == SQLITE_ROW) {
        nextid=sqlite3_column_int(res, 0);
        nextid2=sqlite3_column_int(res, 1);
        //printf("nextid %i\n", sqlite3_column_int(res, 0));
    }
    
    sqlite3_finalize(res);
    if(nextid>nextid2){
        return nextid;
    }else{
        return nextid2;
    }
}

int sqlite_get_compid(){
    sqlite_component_list_create_db_ifnotexists();
    sqlite3_stmt *res;
    int rc;
    rc = sqlite3_prepare_v2(glob_db, "SELECT count(comp_id),max(comp_id)+1 FROM components", -1, &res, 0);    
    if (rc != SQLITE_OK) {
        fprintf(stderr, "sqlite_get_compid: Failed to fetch data1: %s\n", sqlite3_errmsg(glob_db));
        return 1;
    }    
    
    rc = sqlite3_step(res);
    
    int nextid;
    int nextid2;
    
    if (rc == SQLITE_ROW) {
        nextid=sqlite3_column_int(res, 0);
        nextid2=sqlite3_column_int(res, 1);
        //printf("nextid %i\n", sqlite3_column_int(res, 0));
    }
    
    sqlite3_finalize(res);
    if(nextid>nextid2){
        return nextid;
    }else{
        return nextid2;
    }
}

int sqlite_get_pheadid(void){
    return sqlite_get_generic_id("head_id","phead");
}

int sqlite_get_generic_id(char *col,char *table){
    sqlite3_stmt *res;
    int rc;
    char sqlstring[200];
    snprintf(sqlstring,200,"SELECT count(`%s`),max(`%s`)+1 FROM `%s`;",col,col,table);
    rc = sqlite3_prepare_v2(glob_db, sqlstring, -1, &res, 0);    
    if (rc != SQLITE_OK) {
        fprintf(stderr, "sqlite_get_generic_id: Failed to fetch data: %s\n", sqlite3_errmsg(glob_db));
        return 1;
    }    
    
    rc = sqlite3_step(res);
    
    int nextid;
    int nextid2;
    
    if (rc == SQLITE_ROW) {
        nextid=sqlite3_column_int(res, 0);
        nextid2=sqlite3_column_int(res, 1);
        //printf("nextid %i\n", sqlite3_column_int(res, 0));
    }
    
    sqlite3_finalize(res);
    if(nextid>nextid2){
        return nextid;
    }else{
        return nextid2;
    }
}

int sqlite_delete_bom(sqlite3 *db){
    char *err_msg;
    int rc = sqlite3_exec(db, "Delete FROM bom", 0, 0, &err_msg);
    if (rc != SQLITE_OK ) {
        fprintf(stderr, "SQL Insert-error: %s\n", err_msg);
        sqlite3_free(err_msg);        
        //sqlite3_close(db);
        return -1;
    } 
    return 1;
}

int sqlite_open_database(char *dbfile){
    int rc = sqlite3_open(dbfile, &glob_db);
    if (rc != SQLITE_OK) {
        
        fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(glob_db));
        //sqlite3_close(glob_db);
        
        return -3;
    }
    return 1;
}


void sqlite_create_bomtable(void){
    const char sqlcreatedb[]="DROP TABLE IF EXISTS 'bom'; "
        "CREATE TABLE 'bom' (`id`	INTEGER,"
	"`deltaX`	REAL,"
	"`deltaY`	REAL,"
	"`angle`	REAL,"
	"`skip`         INTEGER,"
	"`designator`	TEXT,"
	"`value`	TEXT,"
	"`package`	TEXT,"
	"`side`         TEXT,"
    	"`placement_order` INTEGER"
        ");";
    int sql_result= sqlite3_exec(glob_db, sqlcreatedb, NULL, 0, NULL);  
    //printf("%s\n",sqlcreatedb);
    if (sql_result != SQLITE_OK) {
        printf("sqlite_create_bomtable: Failed to create database: %s\n\r",sqlite3_errstr(sql_result)) ;
        
        return;
    }


}

int sqlite_read_posfile(char *filepath){
    //Reads a KiCad pos file
    sqlite_create_bomtable();
    //first open database
    sqlite3 *db=glob_db;
 
    sqlite_delete_bom(db);
    char *err_msg;
    int rc;
    /*
    rc=sqlite3_exec(db, "BEGIN TRANSACTION;", 0, 0, &err_msg);
    if (rc != SQLITE_OK ) {
        fprintf(stderr, "SQL begin-error: %s\n", err_msg);
        sqlite3_free(err_msg);        
        sqlite3_close(db);
        return -3;
    } */
    //Copy all to memory
    FILE *f = fopen(filepath, "rb");
    if(!f) return -1; //File does not exist
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  //same as rewind(f);

    char *posdata = malloc(fsize + 1);
    fread(posdata, fsize, 1, f);
    fclose(f);
    
    char component[7][100];
    
    char *inserter=malloc(1000000);
    strcpy(inserter,"BEGIN TRANSACTION;DELETE FROM bom;");
    //printf("%s",posdata);
    int fpos=0;
    int idcounter=0;
    int bomid=sqlite_get_bomid(db);

   // int doofercounter=10;
    while(1){
        if(fpos>=fsize) break;
        if(posdata[fpos]=='#'){
            //Line is a comment, avance till we get a new line.
            while(posdata[fpos]!='\n'){
                fpos++;

            }
            fpos++;
            continue;
        }
    
        memset(&component,0,sizeof(component));
        for(idcounter=0;idcounter<7;idcounter++){
            int distancetononslash;
            distancetononslash=distance_to_nonslash(&posdata[fpos]);
            if(distancetononslash>=0){
                
                fpos+=distancetononslash;
            }else{
                fprintf(stderr,"Read file error (distance to non-slash)\n");
                fprintf(stderr,"%s",&posdata[fpos]);
                return -2;
            }

            int charlength;
            charlength=distance_to_slash(&posdata[fpos]);
            if(charlength<0){
                fprintf(stderr,"Read file error (distance to slash)\n");
                fprintf(stderr,"%s",&posdata[fpos]);
                return  -2;
            }
            memcpy(&component[idcounter],&posdata[fpos],charlength);
            fpos+=charlength;
        }
       /* printf("%s|%s|%s|%s|%s|%s|%s\n",
                component[0],component[1],component[2],component[3],component[4],component[5],component[6]);
        */
        //printf("lineend: %i -fpos %i",distance_to_newline_or_fileend(&posdata[fpos]),fpos);
        fpos+=distance_to_newline_or_fileend(&posdata[fpos])+1;
        //printf("%s",&posdata[fpos]);
        
        char sqlstatement[1000];
        snprintf(sqlstatement,1000,
                "INSERT INTO bom "
                "(id,deltaX ,deltaY,angle,skip,designator,value,package,side)"
                "VALUES('%i',\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\");",
                bomid++,
                component[3],
                component[4],
                component[5],
                "6", //skip = nothrow, visual
                component[0],
                component[1],
                component[2],
                component[6]);
        strcat(inserter,sqlstatement);
       // printf(">%s\n",sqlstatement);
        /**/
       

    }
    //printf("test\n");
    strcat(inserter,"UPDATE bom SET skip='1' WHERE designator LIKE 'FID%';");
    strcat(inserter,"COMMIT;");
    //printf("%s\n",inserter);
    rc=sqlite3_exec(db,inserter, 0, 0, &err_msg);
    if (rc != SQLITE_OK ) {
        fprintf(stderr, "SQL Commit-error: %s\n", err_msg);
        sqlite3_free(err_msg);        
        //sqlite3_close(db);
        return -3;
    } 
    free(inserter);
    free(posdata);
    //optimize after insertion directely
    optimalroute_perform();
}


void sqlite_bom_set_placement_order(optrte_t *base){
    printf("updateing placement order in database.\n");
    fflush(stdout);
    //start transaction
    char *err_msg=malloc(100);
    sqlite3_stmt *stmt;
    int sql_result;
    
    sql_result=sqlite3_exec(glob_db, "BEGIN;", NULL, 0, &err_msg);
    if (sql_result != SQLITE_OK) {
        fprintf(stderr,"sqlite_bom_set_placement_order Failed to start transaction %s\n\r",err_msg);
        return;
    }
    
    sql_result= sqlite3_prepare_v2(glob_db,  "update bom set  placement_order=? WHERE id=? ;", -1, &stmt, NULL); 
    if (sql_result != SQLITE_OK) {
        fprintf(stderr,"sqlite_bom_set_placement_order: Failed to start parse insert string %s\n\r",err_msg);
        return;
    }
    
    //cycle through all data
    while(((base)->bom_id)>-1){
        
        sqlite3_bind_int(stmt,
            1,  // Index of wildcard
            base->pos //value
            );
        sqlite3_bind_int(stmt,
            2,  // Index of wildcard
            base->bom_id //value
            );
        if (sqlite3_step(stmt) != SQLITE_DONE) {
            fprintf(stderr,"sqlite_bom_set_placement_order: Could not step (execute) stmt.\n");
            return;
        }
        printf("bomid=%i value=%i\n",base->bom_id,base->pos);
        sqlite3_reset(stmt);
        base++;
    }
    sqlite3_finalize(stmt);
  
    sql_result=sqlite3_exec(glob_db, "COMMIT;", NULL, 0, &err_msg);
    if (sql_result != SQLITE_OK) {
        fprintf(stderr,"sqlite_bom_set_placement_order Failed to commit transaction %s\n",err_msg);
        return;
    }
 
    free(err_msg);
}

void sqlite_component_list_create_db(void){
    sqlite3_stmt *stmt;
    const char sqlcreatedb[]="DROP TABLE IF EXISTS 'components';"
        "CREATE TABLE 'components' ("
	"`comp_id`	INTEGER,"
	"`feeder_id`	INTEGER,"
        "`feeder_width`	INTEGER,"
        "`value`	TEXT,"
        "`package`	TEXT,"
	"`deltaX`	REAL,"
	"`deltaY`	REAL,"
    	"`deltaRot`	REAL,"   
	"`FeedRate`	INTEGER,"
	"`height`	REAL,"
	"`speed`	REAL,"
	"`status`	INTEGER,"
	"`sizeX`	REAL,"
	"`sizeY`	REAL,"
	"`heightTake`	REAL,"
	"`delayTake`	REAL,"
        "`nozzle_id`	INTEGER,"
	"`synonyms`	TEXT"
        ");";
    //printf("%s\n",sqlcreatedb);
    int sql_result= sqlite3_exec(glob_db, sqlcreatedb, NULL, 0, NULL);
    if (sql_result != SQLITE_OK) {
        printf("sqlite_component_list_create_db: Failed to create database: %s\n\r",sqlite3_errstr(sql_result)) ;
        return;
    }
}


void sqlite_phead_add_empty_row(void){
    //INSERT INTO 
    //printf("pheadid: %i\n", sqlite_get_pheadid());
    char insert[300];
    snprintf(insert,300,"INSERT INTO phead (head_id,nozzle_id,description) VALUES(%i,0,'');",sqlite_get_pheadid());
    char *err_msg;
    int sql_result=sqlite3_exec(glob_db, insert, NULL, 0, &err_msg);
    if (sql_result != SQLITE_OK) {
        printf("sqlite_phead_add_empty_row: Failed to insert in component database: %s\nSQL:%s\n",err_msg,insert);
    }
}


void sqlite_component_list_create_db_ifnotexists(void){
    if(sqlite_check_if_table_exists("components")==0){
        sqlite_component_list_create_db();
    } 
}

//Orders and fixes the number of feeders
void sqlite_component_fix_feedernumbers(void){
    sqlite3_stmt *stmt;
    char *sql;
    sql=malloc(1000);
    strcpy(sql,"select comp_id,feeder_id,feeder_width,status%2 as skipit FROM components WHERE feeder_width=1  GROUP BY comp_id HAVING skipit=0 ORDER BY comp_id ");
    int sql_result= sqlite3_prepare_v2(glob_db, sql, -1, &stmt, NULL);   
    char *update_sql=malloc(10000);
    strcpy(update_sql,"BEGIN TRANSACTION;");
    int feeder_id=1;
    do{
        sql_result = sqlite3_step (stmt);  
        if(sql_result != SQLITE_ROW) break;
        if(sqlite3_column_int(stmt,1)!=feeder_id){
            snprintf(sql,1000,"UPDATE components SET feeder_id='%i' WHERE comp_id='%i';",
                   feeder_id,
                   sqlite3_column_int(stmt,0));
            strcat(update_sql,sql);
        }
        feeder_id=feeder_id+sqlite3_column_int(stmt,2);
    }while(1);
    sqlite3_finalize(stmt);
    // 1 space required
    feeder_id++;
    //look for two or more feeders, we place those at the end.
    strcpy(sql,"select comp_id,feeder_id,feeder_width,status%2 as skipit FROM components WHERE feeder_width=2  GROUP BY comp_id HAVING skipit=0 ORDER BY comp_id ");
    sql_result= sqlite3_prepare_v2(glob_db, sql, -1, &stmt, NULL);   
    do{
        sql_result = sqlite3_step (stmt);  
        if(sql_result != SQLITE_ROW) break;
        if(sqlite3_column_int(stmt,1)!=feeder_id){
            snprintf(sql,1000,"UPDATE components SET feeder_id='%i' WHERE comp_id='%i';",
                   feeder_id,
                   sqlite3_column_int(stmt,0));
            strcat(update_sql,sql);
        }
        feeder_id=feeder_id+sqlite3_column_int(stmt,2);
    }while(1);
    sqlite3_finalize(stmt);

    //look for skipped components
    strcpy(sql,"select comp_id,value,feeder_id,feeder_width,status%2 as skipit FROM components GROUP BY comp_id  HAVING skipit>0 ORDER BY comp_id ");
    sql_result= sqlite3_prepare_v2(glob_db, sql, -1, &stmt, NULL);   
    do{
        sql_result = sqlite3_step (stmt);  
        if(sql_result != SQLITE_ROW) break;
        if(sqlite3_column_int(stmt,1)!=feeder_id){
            snprintf(sql,1000,"UPDATE components SET feeder_id='%i' WHERE comp_id='%i';",
                   feeder_id,
                   sqlite3_column_int(stmt,0));
            strcat(update_sql,sql);
        }
        feeder_id=feeder_id+1;
    }while(1);
    sqlite3_finalize(stmt);
    //commit the changes.
    strcat(update_sql,"COMMIT;");
    char *err_msg;
    sql_result=sqlite3_exec(glob_db, update_sql, NULL, 0, &err_msg);
    if (sql_result != SQLITE_OK) {
        fprintf(stderr,"sqlite_component_fix_feedernumbers: Failed to insert in component database: %s\n\r",err_msg);
        
    }
    fprintf(stderr,"%s",update_sql);
    fflush(stderr);
    free(update_sql);
    free(sql);
}
/**/
void sqlite_phead_createdb(void){
    sqlite3_stmt *stmt;
    const char sqlcreatedb[]="CREATE TABLE `phead` ("
	"`head_id`	INTEGER,"
	"`nozzle_id`	INTEGER,"
	"`description`	TEXT"
        ");";
    //printf("%s\n",sqlcreatedb);
    int sql_result= sqlite3_exec(glob_db, sqlcreatedb, NULL, 0, NULL);
    if (sql_result != SQLITE_OK) {
        printf("sqlite_phead_createdb: Failed to create database: %s\n\r",sqlite3_errstr(sql_result)) ;
        return;
    }
}

void sqlite_phead_create_db_ifnotexists(void){
    if(sqlite_check_if_table_exists("phead")==0){
        sqlite_phead_createdb();
    }
}

int sqlite_check_if_table_exists(char* tbl_name){
    sqlite3_stmt *stmt;
    char *sqlstring=malloc(300);
    snprintf(sqlstring,300,"SELECT name FROM sqlite_master WHERE type='table' AND name='%s';",tbl_name);
    int count=0;
    int sql_result= sqlite3_prepare_v2(glob_db, sqlstring, -1, &stmt, NULL);   
    if (sql_result != SQLITE_OK) {
        printf("sqlite_check_if_table_exists: Failed to create database%s\n\r",sqlite3_errstr(sql_result)) ;
    }else{
        do{
            sql_result = sqlite3_step (stmt);  
            if(sql_result != SQLITE_ROW) break;
            count++;
        }while(1);
    }
    
    free(sqlstring);
    return count;
}

void sqlite_component_list_fill(void){
    //fprintf(stderr,"Filling components from bom\n");
    //fflush(stderr);
    sqlite3_stmt *stmt;
    const char sqlfeederstocreate[]="SELECT package,value,count(id) as pcs, skip FROM bom  GROUP BY package, value, skip ORDER BY pcs DESC";
    int sql_result= sqlite3_prepare_v2(glob_db, sqlfeederstocreate, -1, &stmt, NULL);   
    if (sql_result != SQLITE_OK) {
        printf("Failed to create database%s\n\r",sqlite3_errstr(sql_result)) ;
        return;
    }
    
    int comp_id=sqlite_get_compid();
    char *insertstring=malloc(100000);
    char *singlestring=malloc(10000);
    char *value_str=malloc(400);
    char *package_str=malloc(400);
    strcpy(insertstring,"BEGIN TRANSACTION;");
    do{
        sql_result = sqlite3_step (stmt);  
        if(sql_result != SQLITE_ROW) break;
        strcpy(value_str,sqlite3_column_text(stmt,1));
        strcpy(package_str,sqlite3_column_text(stmt,0));
        snprintf(singlestring,10000,
                "INSERT INTO components "
                "(comp_id,feeder_id,feeder_width,value,package,deltaX,deltaY,deltaRot,FeedRate,height,speed,status,sizeX,sizeY,heightTake,delayTake,nozzle_id,synonyms)"
                "VALUES('%i',\"%i\",\"%i\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%i\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\";%s|%s;\");",
                comp_id,
                comp_id+1, //feederid
                1,
                value_str,
                package_str, //feederlabel
                "0", //deltaX
                "0", //deltaY
                "0", //deltaRot
                "4", //feedrate
                "0.5", //height
                "0",//speed
                sqlite3_column_int(stmt,3), //status 11. element
                "0", //sizeX
                "0", //sizeY
                "0", //takeheight
                "0",
                "-1",
                value_str,package_str);
        strcat(insertstring,singlestring);
        
        comp_id++;
    }while(1);
    sqlite3_finalize(stmt);
    strcat(insertstring,"COMMIT;");
    char *err_msg;
    //puts(insertstring);
    sql_result=sqlite3_exec(glob_db, insertstring, NULL, 0, &err_msg);
    if (sql_result != SQLITE_OK) {
        printf("Failed to insert in component database: %s\n\r",err_msg);
        printf("%s",insertstring);
    }
    free(insertstring);
    free(singlestring);
    free(value_str);
    free(package_str);
}

void sqlite_assign_similar_phead(void){
    sqlite3_stmt *stmt;
    char sql[1000];
    strcpy(sql,"SELECT package,phead.nozzle_id FROM phead JOIN components ON(phead.nozzle_id=components.nozzle_id) GROUP BY package");
    int sql_result= sqlite3_prepare_v2(glob_db, sql, -1, &stmt, NULL);   
    char *update_sql=malloc(30000);
    strcpy(update_sql,"BEGIN TRANSACTION;");
    do{
        sql_result = sqlite3_step (stmt);  
        if(sql_result != SQLITE_ROW) break;
        snprintf(sql,1000,"UPDATE components SET nozzle_id='%i' WHERE package='%s';",
                sqlite3_column_int(stmt,1),
                sqlite3_column_text(stmt,0));
        strcat(update_sql,sql);
    }while(1);
    sqlite3_finalize(stmt);
    strcat(update_sql,"COMMIT;");
    char *err_msg;
    sql_result=sqlite3_exec(glob_db, update_sql, NULL, 0, &err_msg);
    if (sql_result != SQLITE_OK) {
        fprintf(stderr,"Failed to insert in component database: %s\n\r",err_msg);
        
    }
    free(update_sql);
}

/*
void sqlite_test(void){
    int retval;
    retval=sqlite_read_posfile("//home/michael/NetBeansProjects/chmt560p4/db/sampleposfile.pos","//home/michael/NetBeansProjects/chmt560p4/db/db.sqlite");
    if(retval<0){
        printf("Error opening file.\n");
    }else{
        printf("Read successfull\n");
    }
}*/
/*
void sqlite_open_database(void){
    sqlite3 *db;
    sqlite3_stmt *res;
    
    int rc = sqlite3_open("//home/michael/NetBeansProjects/chmt560p4/db/db.sqlite", &db);
    
    if (rc != SQLITE_OK) {
        
        fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        
        return;
    }
    
     char *zErrMsg = 0;
     rc = sqlite3_exec(db, "Select id,height FROM feeder Order by id", sqllite_callback_test, NULL, &zErrMsg);
 

}*/

