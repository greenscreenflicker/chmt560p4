/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   sqlite.h
 * Author: michael
 *
 * Created on September 17, 2018, 8:35 PM
 */

#ifndef SQLITE_H
#define SQLITE_H

#ifdef __cplusplus
extern "C" {
#endif

static GtkTreeModel * sqlite_fill_bomlist (void);

void sqlite_component_list_fill(void);
int sqlite_check_if_table_exists(char* tbl_name);
void sqlite_phead_create_db_ifnotexists(void);
void sqlite_component_list_create_db_ifnotexists(void);
void sqlite_component_list_create_db(void);
int sqlite_get_generic_id(char *col,char *table);
int sqlite_get_pheadid(void);
void sqlite_phead_add_empty_row(void);
void sqlite_assign_similar_phead(void);
void sqlite_component_fix_feedernumbers(void);
void sqlite_bom_set_placement_order(optrte_t *base);
int sqlite_read_posfile(char *filepath);
int sqlite_check_if_table_exists(char* tbl_name);
int sqlite_open_database(char *dbfile);

extern sqlite3 *glob_db;

#ifdef __cplusplus
}
#endif

#endif /* SQLITE_H */

