/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   unified.h
 * Author: michael
 *
 * Created on September 15, 2018, 1:50 PM
 */

#ifndef UNIFIED_H
#define UNIFIED_H

#ifdef __cplusplus
extern "C" {
#endif
    
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <alloca.h>
#include <gtk/gtk.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <pwd.h>
//Database    
#include <sqlite3.h>
    
    
#include "mainwindow.h"
#include "optimalroute.h"
#include "sqlite.h"
#include "dpv.h"
#include "helper.h"
#include "feederlist.h"
#include "globalfeeders.h"
#include "fiducial.h"
#include "mainwindow_components.h"




#ifdef __cplusplus
}
#endif

#endif /* UNIFIED_H */

